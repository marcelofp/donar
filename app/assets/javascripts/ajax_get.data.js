var XMLHttpRequestObject = false;
	if (window.XMLHttpRequest) {
		XMLHttpRequestObject = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		XMLHttpRequestObject = new ActiveXObject('Microsoft.XMLHTTP');
	}
	function getData(dataSource, divID)
	{
		
		if(XMLHttpRequestObject) {
			var obj = document.getElementById(divID);
			
			XMLHttpRequestObject.open('GET', dataSource);
			if(obj){
				obj.innerHTML = "<font color='green' size=1 ><b> Aguarde... </b></font><br>";
				obj.innerHTML = "<img width='18px' height='18px' src='view/img/load.gif'> Aguarde, Carregando...";
			}

			XMLHttpRequestObject.onreadystatechange = function()
				{
				
					if (XMLHttpRequestObject.readyState == 4 &&
						XMLHttpRequestObject.status == 200) {
						if(obj){
							
							obj.innerHTML = XMLHttpRequestObject.responseText;
							
						}
					}	
				}
			XMLHttpRequestObject.send(null);
		}
	}

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php 

pop_msg("success","Cadastro de Doação","Efetuado com Sucesso!","home.php?ir=info_animal&id_animal=".$_GET['id'],"");
pop_msg("danger","Cadastro de Doação","Contate o administrador","#","");

?>
	
				



<div id="content-wrapper">

		<div class="page-header">
			<h1><span class="text-light-gray">Doação / </span>Cadastro</h1>
		</div> <!-- / .page-header -->

		<div class="row">
			<div class="col-sm-12">

<!-- 5. $JQUERY_VALIDATION =========================================================================

				jQuery Validation
-->

				
				<script>
				init.push(function () {
					var myDropzone = new Dropzone("#imagens", {
					    
						url: "class/upload_imgtestes.php",
						paramName: "file", // The name that will be used to transfer the file
						addRemoveLinks : true,
						maxFilesize: 3, //Máximo 3mb
						dictResponseError: "Não foi possível enviar o arquivo!",
						parallelUploads: 1,
						maxFiles: 5,
						acceptedMimeTypes: "image/gif, image/jpeg, image/bmp, image/gif, image/png, image/jpe",
						autoProcessQueue: false,
						thumbnailWidth: 138,
						thumbnailHeight: 120,
						previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name></span></div><div class="dz-size">Tamanho: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">Sem vizualização</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
						
						});
					
						// Validação
						$("#jq-validation-form").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: true,
							rules: {
								'tipo': {
								  required: true
								},
								'titulo': {
									required: true,
									maxlength: 200
								},
								'descricao': {
									required: true
									
								}	
								
								
							},

							submitHandler: function(form) {
								$("#ajax-overlay_img").show();	
							    var formdata = $("#jq-validation-form").serialize();										    
						    	$.ajax({
						    		type: "POST",
						    		url: "class/inserir.php?funcao=cad_doacao&idpessoa=<?php echo $id_atual;?>",
						    		data: formdata,
						    		dataType: "json",
						    		success: function(json){
						    			$("#ajax-overlay").hide(); 
						    			if(json.sucesso=="sim"){
							    			if(myDropzone.getQueuedFiles().length>0){
							    				$("#ajax-overlay_img").show();
							    				var qtd = myDropzone.getQueuedFiles().length;
							    				var qtd_andar = 100/qtd;
							    				var porcento = qtd_andar;
							    				var i, b;
							    				b = 1;
							    				myDropzone.on("processing", function(file) {
							    					 qtd = qtd +1;
								    			      this.options.url = "class/upload_imgteste.php?id="+json.id;
								    			      this.options.autoProcessQueue = true;
								    			      window.location.hash="#enviar";
								    			      for(i = b; i<=porcento; i++){
								    			    	  $('#div_porcento').css('width', i+'%');
									    			      }
								    			      b = porcento;		  
								    			      porcento = porcento + qtd_andar;
								    			      
								    			    });										    						
								    				myDropzone.processQueue();
								    				
								    				myDropzone.on("queuecomplete", function (file) {
								    					$("#ok_modal").attr("href", "home.php?ir=doacao&iddoar="+json.id);
								    					$("#ajax-overlay_img").hide();
								    					$('#pop_modal_success').modal('show').data('bs.modal').isShown = false;				
								    			      });	
								    			    }else{
								    			    $("#ok_modal").attr("href", "home.php?ir=doacao&iddoar="+json.id);	
							    					$('#pop_modal_success').modal('show').data('bs.modal').isShown = false;			
							    						
									    			}
							    			}else{
						    			$('#pop_modal_danger').modal('show');	
							    		}
						    		}
						    		});
					        }
							
						});
					});
				</script>
				<!-- / Javascript -->

				<div class="panel panel-success">
					<div class="panel-heading">
						<span class="panel-title">Formulário de Cadastro</span>
					</div>
					
				
					<div class="panel-body ">
						<div class="note note-success">Para doar mais rápido insira o título e descrição detalhados;
Fotos originais e em diferentes ângulos; </div>
						
						<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data"  id="jq-validation-form">
						<BR>
						<div class="table-footer">
                            <div  class="label  label-tag label-success">   Dados da doação</div>
                            
                            <div class="form-group">
								<label for="jq-validation-password" class="col-sm-3 control-label">Tipo?</label>
								<div class="col-sm-9">
									<select class="form-control" name="tipo" id="tipo">
									<option value="1">Doação</option>
									<option value="2">Solicitação</option>
									<option value="3">Doação Voluntária</option>
									</select>
								</div>
							</div>
                            
							<div class="form-group">
							<div  id="tooltips-demo">
								<label for="jq-validation-required2" class="col-sm-3 control-label">Título 
								 </label>
								</div>
								<div class=" tooltips-demo col-sm-9">
									<input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título da doação...">
								</div>
							</div>					
									
							
							
							<div class="form-group">
								<label for="jq-validation-select3" class="col-sm-3 control-label">Descrição</label>
								<div class="col-sm-9">
									<textarea id="character-limit-textarea" name="descricao" rows="3" class="form-control"></textarea>
						<div id="character-limit-textarea-label" class="limiter-label">Caracteres restantes: <span class="limiter-count"></span></div>
								</div>
							</div>
						</div>
						<BR><BR>
						<div class="table-footer">
                            <div  class="label  label-tag label-success">   Imagens da Doação</div>
							<div class="form-group" id="tooltips-demo">
								<label for="jq-validation-select3" class="col-sm-3 control-label">Imagens da Doação
												<a href="#" class="tooltip-info" data-toggle="tooltip" data-placement="top" data-original-title="Máximo 5 imagens com 3mb cada."><i class="fa fa-question-circle text-info alert-dark"></i></a>
						
								</label>
								<div class="col-sm-9">
									<div id="imagens" class="dropzone-box">
							<div class="dz-default dz-message">
								<i class="fa fa-cloud-upload"></i>
								Arraste os arquivos<br><span class="dz-text-small">ou clique para inserir as imagens</span>
							</div>
							
								
								<div class="fallback">
									<input style="min-height: 800" name="file" type="file" multiple="" />
								</div>
							
								
								
							
						</div>
								</div>
							</div>

</div><BR>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9"> 	
									<button id="enviar" class="btn btn-success"><i class="fa fa-plus"> </i> Cadastrar</button>
								</div>
							</div>
						</form>
				</div> </div></div></div></div>
				
<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	   <meta property="og:url"           content="http://www.donar.com.br" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Donar! Ajude quem ajuda..." />
    <meta property="og:description"   content="Quer praticar a soliedariedade? Desapegue com o DONAR!" />
    <meta property="og:image"         content="http://www.donar.com.br/app/view/img/donar.png" />
	<title>DONAR - Ajude quem ajuda!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
 <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
	<!-- Open Sans font from Google CDN -->
	
	<!-- Pixel Admin's stylesheets -->
	<link href="assets/stylesheets/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/pixel-admin.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/widgets.min.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/pages.min.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/themes.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/principal.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/jquery.fancybox.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/flexslider.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/dataTables.responsive.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
		<script src="assets/javascripts/ie.min.js"></script>
	<![endif]-->

</head>
<?php 
require "class/Conexao.class.php";
require "class/funcoes.php";


?>
<?php
$id_atual=3;
$var = "Thiago Chaves";//$_SESSION['nome_sizo'];
$nome = explode(" ", $var);

$filename = 'view/img/avatars/'.$id_atual.'/'.$id_atual.'p.jpg';

if (file_exists($filename)) {
  $imagem="view/img/avatars/".$id_atual."/".$id_atual."p.jpg";
} else {
   $imagem="view/img/avatars/padrao.jpg";
}
?>

<body class="theme-adminflare main-menu-animated page-profile main-menu-fixed main-navbar-fixed" >

<script>var init = [];</script>
<div id="main-wrapper">


<!-- 2. $MAIN_NAVIGATION ===========================================================================

	Main navigation
-->
	<div id="main-navbar" class="navbar navbar-inverse" role="navigation">
		<!-- Main menu toggle -->
		<button type="button" id="main-menu-toggle"><i class="navbar-icon fa fa-bars icon"></i><span class="hide-menu-text">ESCONDER MENU</span></button>
		
		<div class="navbar-inner">
			<!-- Main navbar header -->
			<div class="navbar-header">

				<!-- Logo -->
				<a href="home.php" class="navbar-brand">
					<div><img alt="Donar - Ajude quem ajuda!" src="view/img/donar.png"></div>
					DONAR
				</a>

				<!-- Main navbar toggle -->
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse"><i class="navbar-icon fa fa-bars"></i></button>

			</div> <!-- / .navbar-header -->

			<div id="main-navbar-collapse" class="collapse navbar-collapse main-navbar-collapse">
				<div>
					

					<div class="right clearfix">
						<ul class="nav navbar-nav pull-right right-navbar-nav">

								<script>
									init.push(function () {
										$('#main-navbar-notifications').slimScroll({ height: 250 });
									});
								</script>
								<!-- / Javascript -->

								<div class="dropdown-menu widget-notifications no-padding" style="width: 300px">
								
									<div class="notifications-list" id="main-navbar-notifications">
									
									
								    	
										

									</div> <!-- / .notifications-list -->
									<a href="#" class="notifications-link">Mais Log´s</a>
								</div> <!-- / .dropdown-menu -->
							</li>
							

							<li class="dropdown">
								<a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown">
									<img src="<?php echo $imagem; ?>" alt="">
									<span><?php  echo $nome[0]." ". $nome[count($nome)-1]; ?></span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="?ir=info_pessoa&idpessoa=<?php echo $id_atual?>"><i class="fa fa-smile-o"></i>&nbsp;&nbsp;Meu Perfil</a></li>
									<li><a href="?ir=info_pessoa&idpessoa=<?php echo $id_atual?>&tab=avatar"><i class="fa fa-picture-o"></i>&nbsp;&nbsp;Alterar Imagem</a></li>
									<li><a href="?ir=info_pessoa&idpessoa=<?php echo $id_atual;?>&tab=senha"><i class="dropdown-icon fa fa-lock"></i>&nbsp;&nbsp;Alterar Senha</a></li>
									<li class="divider"></li>
									<li><a href="class/sair.php"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Sair</a></li>
								</ul>
							</li>
						</ul> <!-- / .navbar-nav -->
					</div> <!-- / .right -->
				</div>
			</div> <!-- / #main-navbar-collapse -->
		</div> <!-- / .navbar-inner -->
	</div> <!-- / #main-navbar -->

-->
	<div id="main-menu" role="navigation">
		<div id="main-menu-inner">
			<div class="menu-content top" id="menu-content-demo">
				<!-- Menu custom content demo
					 CSS:        styles/pixel-admin-less/demo.less or styles/pixel-admin-scss/_demo.scss
					 Javascript: html/assets/demo/demo.js
				 -->
				<div>
					<div class="text-bg"><span class="text-slim">Bem vindo,</span> <span class="text-semibold"><BR><?php echo $nome[0]; ?></span></div>

					<img src="<?php echo $imagem; ?>" alt="" class="">
					<div id="tooltips-demo" class="btn-group">
					
						<a href="?ir=info_pessoa&idpessoa=<?php echo $id_atual?>" class="btn btn-xs btn-primary btn-outline dark tooltip-info"  data-toggle="tooltip" data-placement="top" data-original-title="Meu Perfil"><i class="fa fa fa-smile-o"></i></a>
						<a href="?ir=info_pessoa&idpessoa=<?php echo $id_atual?>&tab=avatar" class="btn btn-xs btn-primary btn-outline dark tooltip-info"  data-toggle="tooltip" data-placement="top" data-original-title="Alterar Imagem"><i class="fa fa-picture-o"></i></a>
						<a href="?ir=info_pessoa&idpessoa=<?php echo $id_atual;?>&tab=senha" class="btn btn-xs btn-primary btn-outline dark tooltip-info"  data-toggle="tooltip" data-placement="top" data-original-title="Alterar Senha"><i class="fa fa fa-lock"></i></a>
						<a href="class/sair.php" class="btn btn-xs btn-danger btn-outline dark tooltip-danger"  data-toggle="tooltip" data-placement="top" data-original-title="Sair"><i class="fa fa-power-off"></i></a>
					</div>
					<a href="#" class="close">&times;</a>
				</div>
			</div>
			<ul class="navigation">
				<li>
					<a href="home.php"><span class="mm-text"><img src="view/img/icones/house.png"> Inicial</span></a>
				</li>
				<li class="mm-dropdown">
					<a href="?ir=cad_doacao"><img width="16px"  src="view/img/donar.png"> <span class="mm-text">Donar</span></a>
					
				</li>
				
				
				<li class="mm-dropdown">
							<a href="?ir=solicitacoesatendidas"><img src="view/img/icones/report.png"> <span class="mm-text">Minhas Publicações</span></a>
							
						</li>
			
				<li class="mm-dropdown"><a href="?ir=info_pessoa&idpessoa=<?php echo $id_atual?>"><img src="view/img/icones/user.png"><span class="mm-text"> Meus Detalhes</span></a>
							
						</li>
						
			<li class="mm-dropdown">
							<a href="?ir=ranking"><img width="16px"  src="view/img/icone-donar_0002_ouro.png"> <span class="mm-text">Ranking de Donações</span></a>
						
				
				
				
				</li>
				<ul>
				
		</div> <!-- / #main-menu-inner -->
	</div> <!-- / #main-menu -->
<!-- /4. $MAIN_MENU -->


<!-- 5. $CONTENT ===================================================================================

		Content
-->

		<!-- Content here -->
		<? 
		
		$ir = $_GET['ir'];
$ext = (isset($_GET['ext']));
if (empty($ext)) $ext="php";
if (empty($ir)) $ir = "main.php"; else $ir .= ".".$ext;
if (file_exists($ir)) include $ir;  else{ include("view/404.php"); $p404 = true;}
?>

	<div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- Pixel Admin's javascripts -->
<script src="assets/javascripts/bootstrap.min.js"></script>
<script src="assets/javascripts/pixel-admin.js"></script>
<script src="assets/javascripts/bootstrap-hover-dropdown.js"></script>
<script src="assets/javascripts/ajax_get.data.js"></script>
<script src="assets/javascripts/jquery.ba-dotimeout.js"></script>
<script src="assets/javascripts/jquery.fancybox.js"></script>
<script src="assets/javascripts/dataTables.responsive.js"></script>
<script src="assets/javascripts/jquery.flexslider.js"></script>
<script src="assets/javascripts/livequery.js"></script>
<script src="assets/javascripts/projetouniversal.js"></script>
<script src="class/funcoes.js"></script>



<script type="text/javascript">
	init.push(function () {

		// Javascript code here
	});
	window.PixelAdmin.start(init);
	$("body").keypress(function(e) {
    	if (e.which == 13 && !$(e.target).is("textarea") && !$(e.target).is("#lol")) { 
    		return false;
    	}
    });

	
	$(document).unbind('keydown').bind('keydown', function (event) {
	    var doPrevent = false;
	    if (event.keyCode === 8) {
	        var d = event.srcElement || event.target;
	        if ((d.tagName.toUpperCase() === 'INPUT' && 
	             (
	                 d.type.toUpperCase() === 'TEXT' ||
	                 d.type.toUpperCase() === 'PASSWORD' || 
	                 d.type.toUpperCase() === 'FILE' || 
	                 d.type.toUpperCase() === 'EMAIL' || 
	                 d.type.toUpperCase() === 'SEARCH' || 
	                 d.type.toUpperCase() === 'DATE' )
	             ) || 
	             d.tagName.toUpperCase() === 'TEXTAREA') {
	            doPrevent = d.readOnly || d.disabled;
	        }
	        else {
	            doPrevent = true;
	        }
	    }
	
	    if (doPrevent) {
	        event.preventDefault();
	    }
	});
	
	//------------------------------------------------------------
    // 	Ajax Indicator
    //------------------------------------------------------------
    //Tooltip dos botões.
    $('#tooltips-demo button').tooltip();
    $('#tooltips-demo a').tooltip();
    $('.flexslider').flexslider({
        animation: "slides",
        animationLoop: true,
        itemWidth: 500,
        itemHeight: 300,
        itemMargin: 0
      });
    $(".fancybox").fancybox();
	
</script>

<div id="ajax-overlay"><div id="processando" ><span><img width="20px" src="view/img/load.gif"> Processando...<br><font color=gray size=1">Por favor, aguarde um instante</font></span></div></div> 
<div id="ajax-overlay_img"><div id="processando" ><span><img width="20px" src="view/img/load.gif"><font color=green> Enviando Arquivos...</font><br><div id="envioImg">
<div class="progress progress-striped active" align="center" style="height:20px;"><div class="progress-bar progress-bar-success" id="div_porcento" align="center" style="width: 1%;">
</div></div></div><font color=gray size=1">Por favor, aguarde um instante</font></span></div></div> 

</body>
</html>
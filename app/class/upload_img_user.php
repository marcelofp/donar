<?php
require_once ("Upload.php");   // incluindo a classe util

$id_galeria = $_GET["idpessoa"];

if (!empty($_FILES)) {
    $adm_caminho = "../view/img/avatars/{$id_galeria}";
    $delete1 = "../view/img/avatars/{$id_galeria}/{$id_galeria}p.jpg";
    $delete2 = "../view/img/avatars/{$id_galeria}/{$id_galeria}g.jpg";
    $nome = $id_galeria;

    $dimenssoes = array("p" => 300, "g" => 600);

    $file = new Upload($_FILES['arquivo']);
    if ($file->uploaded) {
        $file->image_resize = true;
        //$file->image_ratio_x = true;
        $file->image_ratio_crop = true;
        $file->image_convert = 'jpg';
        unlink($delete1);
        unlink($delete2);

        foreach($dimenssoes as $n => $d)
        {
            $file->file_new_name_body = $nome.$n;
            $file->image_x = $d;
            $file->image_y = $d;
            $file->process($adm_caminho);
        }

        $file->clean();

?>
        <script type="text/javascript">
            alert('Imagem Alterada com Sucesso!');
            document.location = '../home.php?ir=info_pessoa&idpessoa=<?php print $id_galeria; ?>';
        </script>
<?php
    }else {
        echo 'error : ' . $file->error;
    }

    
}


?>

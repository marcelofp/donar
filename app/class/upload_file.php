<?php
header('Cache-Control: no-cache, must-revalidate');
header('Content-Type: application/json; charset=utf-8');


$id = $_GET['id'];
// Pasta onde o arquivo vai ser salvo
$_UP['pasta'] = "../view/img/protocolo/{$id}/";
mkdir($_UP['pasta'], 0700);


// Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
$_UP['renomeia'] = true;
$extensao = strtolower(end(explode('.', $_FILES['file']['name'])));
$str = "abcdefg123456";
$codigo = str_shuffle($str);
// O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta

// Primeiro verifica se deve trocar o nome do arquivo
if ($_UP['renomeia'] == true) {
  // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
  $nome_final = $codigo.".".$extensao;
} else {
  // Mantém o nome original do arquivo
  $nome_final = $_FILES['file']['name'];
}
  
// Depois verifica se é possível mover o arquivo para a pasta escolhida
if (move_uploaded_file($_FILES['file']['tmp_name'], $_UP['pasta'] . $nome_final)) {
  
} else {
  
}

?>

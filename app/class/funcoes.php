<?
function muda_data($data){
	
	
}
function mascara($val, $mask)
{
	/* Exemplo de funcionamento da mascara
	 *  $cnpj = "11222333000199";
		$cpf = "00100200300";
		$cep = "08665110";
		$data = "10102010";
		
		echo mask($cnpj,'##.###.###/####-##');
		echo mask($cpf,'###.###.###-##');
		echo mask($cep,'#####-###');
		echo mask($data,'##/##/####');
	 */ 
	$maskared = '';
	$k = 0;
	for($i = 0; $i<=strlen($mask)-1; $i++)
	{
		if($mask[$i] == '#')
		{
			if(isset($val[$k]))
				$maskared .= $val[$k++];
		}
		else
		{
			if(isset($mask[$i]))
				$maskared .= $mask[$i];
		}
	}
	return $maskared;
}

function pop_msg($tipo, $titulo, $mensagem, $link, $permissao){
	if($tipo == "success"){
		$img = "check";
		$redirect = $link;
	}
	if($tipo == "danger"){
		$img = "times";
	}
	$redirect = $link;
	echo '
	<div id="pop_modal_'.$tipo.$permissao.'" class="modal modal-alert modal-'.$tipo.' fade" >
			<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
										<i class="fa fa-'.$img.'-circle"></i>
							</div>
							<div class="modal-title">'.$titulo.'</div>
							<div id="mensagem_modal" class="modal-body">'.$mensagem.'</div>
							<div class="modal-footer">
									
							
								<a href="'.$redirect.'" id="ok_modal'.$permissao.'"><button type="button" class="btn btn-'.$tipo.'" data-dismiss="modal">OK</button></a>
							</div>
						</div> <!-- / .modal-content -->
			</div> <!-- / .modal-dialog -->
</div> <!-- / .modal -->
												
	
';
}

function geraTimestamp($data) {
	$partes = explode('-', $data);
	return mktime(0, 0, 0, $partes[1], $partes[2], $partes[0]);
}
//

function calcula_datas($data_inicial,$data_ultimo_bkp){
	$time_inicial = geraTimestamp($data_inicial);
	$time_final = geraTimestamp($data_ultimo_bkp);
	
	// Calcula a diferença de segundos entre as duas datas:
	$diferenca = $time_final - $time_inicial; 
	
	// Calcula a diferença de dias
	$dias = (int)floor( $diferenca / (60 * 60 * 24)); 
	return $dias;
	
	
}

?>
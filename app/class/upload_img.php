<?php
require_once ("Upload.php");   // incluindo a classe util

$id_galeria = $_GET["id_usuario"];

if (!empty($_FILES)) {
    $adm_caminho = "../view/img/avatars/{$id_galeria}";
    $nome = ( (date("H") *60) * (date("i")*60) + date("s") ).rand(0,1000000).date("d"."m"."Y").str_replace(".","",getenv("REMOTE_ADDR"));

    $dimenssoes = array("p" => 150, "g" => 600);

    $file = new Upload($_FILES['arquivo']);
    if ($file->uploaded) {
        $file->image_resize = true;
        //$file->image_ratio_x = true;
        $file->image_ratio_crop = true;
        $file->image_convert = 'jpg';


        foreach($dimenssoes as $n => $d)
        {
            $file->file_new_name_body = $nome.$n;
            $file->image_x = $d;
            $file->image_y = $d;
            $file->process($adm_caminho);
        }

        $file->clean();

?>
        <script type="text/javascript">
            alert('Imagem Alterada com Sucesso!');
            document.location = '?prefixo=gal&acao=gerenciar&id_galeria=<?php print $id_galeria; ?>';
        </script>
<?php
    }else {
        echo 'error : ' . $file->error;
    }

    
}


?>

<?
//////////////Consulta da página cad_animal.php//////////////////////
if($_GET['funcao']=="consultaRacas") con_racas($_GET['especie_id']);
/////////////////////////////////////////////////////////////////////

//////////////Consulta da página cad_proprietario.php//////////////////////
if($_GET['funcao']=="consultaCidades") con_cidades($_GET['estado_id']);
/////////////////////////////////////////////////////////////////////


//////////////Consulta da página con_animal.php//////////////////////
if($_GET['funcao']=="con_animal_nome") con_animal($_GET['param'], $_GET['funcao'], 3);
if($_GET['funcao']=="con_animal_codigo") con_animal($_GET['param'], $_GET['funcao'], 6);
if($_GET['funcao']=="con_animal_sizo") con_animal($_GET['param'], $_GET['funcao'], 5);
if($_GET['funcao']=="con_animal_proprietario_cpf") con_animal($_GET['param'], $_GET['funcao'], 11);
/////////////////////////////////////////////////////////////////////

//////////////Consulta da página con_protocolo.php//////////////////////
if($_GET['funcao']=="con_protocolo_codigo") con_protocolo($_GET['param'], $_GET['funcao'], 4);
if($_GET['funcao']=="con_protocolo_cpf") con_protocolo($_GET['param'], $_GET['funcao'], 11);
if($_GET['funcao']=="con_protocolo_cnpj") con_protocolo($_GET['param'], $_GET['funcao'], 14);
if($_GET['funcao']=="con_protocolo_solicitante") con_protocolo($_GET['param'], $_GET['funcao'], 3);
/////////////////////////////////////////////////////////////////////

//////////////Consulta da página con_proprietario.php//////////////////////
if($_GET['funcao']=="con_proprietario_nome") con_proprietario($_GET['param'], $_GET['funcao'], 3);
if($_GET['funcao']=="con_proprietario_animal_codigo") con_proprietario($_GET['param'], $_GET['funcao'], 5);
if($_GET['funcao']=="con_proprietario_sizo") con_proprietario($_GET['param'], $_GET['funcao'], 5);
if($_GET['funcao']=="con_proprietario_cpf_") con_proprietario($_GET['param'], $_GET['funcao'], 11);
if($_GET['funcao']=="con_animal_vinculo" or $_GET['funcao']=="con_animal_vinculo_rga") con_animal_vinculo($_GET['param'], $_GET['funcao'], 5);
/////////////////////////////////////////////////////////////////////

///////////////Consulta página vinculação de proprietário ao animal//////////////////
if($_GET['funcao']=="con_proprietario_vinculo_cpf" or $_GET['funcao']=="con_proprietario_vinculo_sizo") con_proprietario_vinculo($_GET['param'], $_GET['funcao'], 5);
////////////////////////////////////////////////////////////////////////////////////

//////////////////////////Consulta da página cad_animal.php////////////////////
if($_GET['funcao']=="con_proprietario_cpf") con_proprietario_cpf($_GET['cpf']);
///////////////////////////////////////////////////////////////////////////////

function con_option($tabela){
	require "class/Conexao.class.php";
	$sql = 'SELECT * from '.$tabela.' ORDER BY descricao';
	foreach ($conn->query($sql) as $row) {
		if($tabela=="setor" or $tabela == "cargo") $descricao = $row['descricao']; else $descricao = htmlentities($row['descricao']);
		echo '<option value="'.$row['id'].'">'.$descricao.'</option>';
	}	
		
}

function con_setor_origem($id){
	require "class/Conexao.class.php";
	$sql = 'SELECT * from setor where id='.$id;
	foreach ($conn->query($sql) as $row) {		
		echo '<option selected value="'.$row['id'].'">'.$row['descricao'].'</option>';
	}

}

function con_usuario(){
	require "class/Conexao.class.php";
	$sql = 'SELECT * from usuario where deletado=0';
	foreach ($conn->query($sql) as $row) {
		echo '<option value="'.$row['usuario'].'">'.$row['usuario']." - ".$row['nome'].'</option>';
	}

}

function con_usuario_id(){
	require "class/Conexao.class.php";
	$sql = 'SELECT * from usuario where deletado=0';
	foreach ($conn->query($sql) as $row) {
		echo '<option value="'.$row['id'].'">'.$row['usuario']." - ".$row['nome'].'</option>';
	}

}



function con_racas($especie_id){
	sleep(2);
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-Type: application/json; charset=utf-8');
	require "Conexao.class.php";
	$sql = 'SELECT * from raca where especie_id='.$especie_id.' ORDER BY descricao';
	foreach ($conn->query($sql) as $row) {
		 $raca[] = array(
        'cod_raca'   => $row['id'],
        'descricao'          => htmlentities($row['descricao']),
    );
	}
	
	echo( json_encode( $raca ) );

}

function con_cidades($estado_id){
	sleep(2);
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-Type: application/json; charset=utf-8');
	require "Conexao.class.php";
	$sql = 'SELECT * from cidade where estado_id='.$estado_id.' ORDER BY descricao';
	foreach ($conn->query($sql) as $row) {
		 $cidade[] = array(
        'cod_cidade'   => $row['id'],
        'descricao'          => htmlentities($row['descricao']),
    );
	}
	
	echo( json_encode( $cidade ) );

}

function con_proprietario_cpf($cpf){
	sleep(1);
	$cpf=preg_replace( '#[^0-9]#', '',$_GET['cpf']);
	if(strlen($cpf)<11){ echo ""; }
	else{
	require "Conexao.class.php";
	require "funcoes.php";
	$result = $conn->prepare("SELECT id, nome, cpf FROM proprietario WHERE cpf= :cpf and deletado='0'");
$result->bindParam(':cpf', $cpf);
$result->execute();
$linha = $result->fetch(PDO::FETCH_ASSOC);
if($linha['cpf']==NULL){
	$linha['cpf']="nao";
}
if($linha['cpf']==$cpf){
	
		?>
		<br>
		<div style="max-width: 300px"  > 
	<div class="btn-success btn-flat btn-sm"align="left"  style="width:120pX; -moz-border-radius:7px 7px 0px 0px;
-webkit-border-radius:7px 7px 0px 0px;
 border-radius:7px 7px 0px 0px;"><i class="fa fa-user"></i> Proprietário</div>
	
	<div class="btn-sm" style="border: 1px solid #ccc; -moz-border-radius:0px 7px 7px 0px;
-webkit-border-radius:0px 7px 7px 0px;
 border-radius:0px 7px 7px 0px; background-color:#FCEFC9;">
		Nome: <strong><? echo $linha['nome'] ?></strong> <br> 
			CPF: <strong> <? echo mascara($linha['cpf'],'###.###.###-##'); ?></strong>
		</div>
		<div> 
		<a target="_blank" href="?ir=info_proprietario&id_proprietario=<? echo $linha['id'] ?>"  id="incluir" style="-moz-border-radius:0px 0px 7px 7px;
-webkit-border-radius:0px 0px 7px 7px;
 border-radius:0px 0px 7px 7px;" class="btn btn-flat btn-sm btn-info"><span id="incluir" class="btn-label  icon fa fa-search"></span> Mais Detalhes...</a>
					
		</div>
	
	
	</div>
	
	<?
		
		
	}else{
		
		echo '<div  class="alert alert-danger alert-dark" style="width:300px; height:30px; padding-top: 4px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							Proprietário não encontrado...
						</div>';
	}
	

	}
}

function con_animal_vinculo(){
sleep(1);
$id=preg_replace( '#[^0-9]#', '',$_GET['param']);
	if(strlen($id)<5) echo "Digite no mínimo 5 caracteres...";
	else{ ?> <?php 
		require "Conexao.class.php";
		require "funcoes.php";
		if($_GET['funcao']=="con_animal_vinculo")$result = $conn->prepare("SELECT id, nome, Proprietario_cpf, codigo FROM animal WHERE id=:id and deletado='0'");
		else $result = $conn->prepare("SELECT id, nome, Proprietario_cpf, codigo FROM animal WHERE codigo=:id and deletado='0'");
		$result->bindParam(':id', $id);	
		$result->execute();
		$linha = $result->fetch(PDO::FETCH_ASSOC);
		
		if($linha['id']==$id or $linha['codigo']==$id){
			?>
			<script>
	$('#cancelarEditar').click(function(){
			$('#div_vazia_vinculo').hide();
		});
	
	$('#vincular_animal').click(function(){
		$("#ajax-overlay").show();
	$.ajax({
		type: "POST",
		url: "class/alterar.php?funcao=alt_vinculo_animal&id_animal=<?php echo $linha['id']?>&id_resp=<?php echo $_GET['id_resp'];?>&resp_usuario=<?php echo $_GET['resp_usuario']; ?>&cpf=<?php echo $_GET['cpf']?>",
		dataType: "json",
		success: function(json){
			if(json.sucesso=="sim"){
	    		$("#ajax-overlay").hide();
	    		$("#ok_modal_editar_vinculo").attr("href", "home.php?ir=info_proprietario&id_proprietario=<?php echo $_GET['id_proprietario'];?>&tab=animais");
				$('#pop_modal_success_editar_vinculo').modal('show');
			 }else{
				 $("#ajax-overlay").hide();	
				 $("#pop_modal_danger_editar_erro_vinculo").modal('show');	
			 	}
		}
		});
	});
	
	</script>
			 <?php 
			if($linha['Proprietario_cpf']!=NULL or $linha['Proprietario_cpf']!=""){
				if($linha['Proprietario_cpf']==$_GET['cpf']){
					echo'<BR><div  class="alert alert-danger alert-dark" style="width:300px; padding-top: 4px;">
								<button type="button" class="close" data-dismiss="alert">×</button>
								Este animal já é vinculado ao proprietário que você deseja vincular.
							</div><BR><a id="cancelarEditar" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>';
				}else{
				echo'<BR><div  class="alert alert-danger alert-dark" style="width:300px; padding-top: 4px;">
								<button type="button" class="close" data-dismiss="alert">×</button>
								Este animal já tem um proprietário: <BR>CPF: <strong>'.mascara($linha['Proprietario_cpf'],"###.###.###-##").'</strong>
								Para prosseguir com a vinculação é necessário que se desvincule o animal do CPF listado.
							</div><BR><a id="cancelarEditar" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>';
				}
			}else{
			?>
			<br>
			<div style="max-width: 300px"  > 
		<div class="btn-success btn-flat btn-sm"align="left"  style="width:120pX; -moz-border-radius:7px 7px 0px 0px;
	-webkit-border-radius:7px 7px 0px 0px;
	 border-radius:7px 7px 0px 0px;"><i class="fa fa-github-alt"></i> Animal</div>
		
		<div class="btn-sm" style="border: 1px solid #ccc; -moz-border-radius:0px 7px 7px 0px;
	-webkit-border-radius:0px 7px 7px 0px;
	 border-radius:0px 7px 7px 0px; background-color:#FCEFC9;">
				<?php 
				$fotos_animal_p = glob("../view/img/animal/".$linha['id']."/*p.jpg");
				$fotos_animal_g = glob("../view/img/animal/".$linha['id']."/*g.jpg");
				if (file_exists($fotos_animal_p[0])) {
					$imagem=substr($fotos_animal_p[0], 3);
					$imagem2=substr($fotos_animal_g[0], 3);
				} else {
					$imagem="view/img/noimg.png";
					$imagem2="view/img/noimg.png";
				}
				?>
	 			<div style="float:right;"><a href="<?php echo $imagem2 ?>" data-fancybox-group="gallery" class="dropdown-toggle user-menu fancybox" data-toggle="dropdown">
									<img class="rounded" width="60px" src="<?php echo $imagem ?>"></a></div> 
				Nome: <strong><? echo $linha['nome']; ?></strong><br> 
				Código Sizo: <strong> <? echo $linha['id']; ?></strong><BR>
				Código RGA: <strong> <? echo $linha['codigo']; ?></strong>
			</div>
			<div> 
			<a target="_blank" href="?ir=info_animal&id_animal=<? echo $linha['id']; ?>"  id="incluir" style="-moz-border-radius:0px 0px 7px 7px;
	-webkit-border-radius:0px 0px 7px 7px;
	 border-radius:0px 0px 7px 7px;" class="btn btn-flat btn-sm btn-info"><span id="incluir" class="btn-label  icon fa fa-search"></span> Mais Detalhes...</a>
						
			</div>
		
		
		</div><BR>
		
		<a id="vincular_animal" class="btn btn-success"><i class="fa fa-plus"> </i> Vincular</a>
		&nbsp&nbsp&nbsp<a id="cancelarEditar" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
		
		<?
			}	
			
		}else{
			
			echo '<BR><div  class="alert alert-danger alert-dark" style="width:300px; height:30px; padding-top: 4px;">
								<button type="button" class="close" data-dismiss="alert">×</button>
								Animal não encontrado...
							</div><BR><a id="cancelarEditar" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>';
		}
		
	
		}
	
}

function con_proprietario_vinculo(){
	sleep(1);
	$id=preg_replace( '#[^0-9]#', '',$_GET['param']);
	if(strlen($id)<5) echo "Digite no mínimo 5 caracteres...";
	else{ ?> <?php
		require "Conexao.class.php";
		require "funcoes.php";
		if($_GET['funcao']=="con_proprietario_vinculo_sizo")$result = $conn->prepare("SELECT id, nome, cpf FROM proprietario WHERE id=:id and deletado='0'");
		else $result = $conn->prepare("SELECT id, nome, cpf FROM proprietario WHERE cpf=:id and deletado='0'");
		$result->bindParam(':id', $id);	
		$result->execute();
		$linha = $result->fetch(PDO::FETCH_ASSOC);
		
		if($linha['id']==$id or $linha['cpf']==$id){
			?>
			<script>
	$('#cancelarEditar').click(function(){
			$('#div_vazia').hide();
		});
	
	$('#vincular_animal').click(function(){
		$("#ajax-overlay").show();
	$.ajax({
		type: "POST",
		url: "class/alterar.php?funcao=alt_vinculo_proprietario&cpf_proprietario=<?php echo $linha['cpf']?>&id_resp=<?php echo $_GET['id_resp'];?>&id_animal=<?php echo $_GET['id_animal']?>",
		dataType: "json",
		success: function(json){
			if(json.sucesso=="sim"){
	    		$("#ajax-overlay").hide();
	    		$("#ok_modal_editar_vinculo").attr("href", "home.php?ir=info_animal&id_animal=<?php echo $_GET['id_animal']?>");
				$('#pop_modal_success_editar_vinculo').modal('show');
			 }else{
				 $("#ajax-overlay").hide();	
				 $("#pop_modal_danger_editar_erro_vinculo").modal('show');	
			 	}
		}
		});
	});
	
	</script>
			 
			<br>
			<div style="max-width: 300px"  > 
		<div class="btn-success btn-flat btn-sm"align="left"  style="width:120pX; -moz-border-radius:7px 7px 0px 0px;
	-webkit-border-radius:7px 7px 0px 0px;
	 border-radius:7px 7px 0px 0px;"><i class="fa fa-github-alt"></i> Animal</div>
		
		<div class="btn-sm" style="border: 1px solid #ccc; -moz-border-radius:0px 7px 7px 0px;
	-webkit-border-radius:0px 7px 7px 0px;
	 border-radius:0px 7px 7px 0px; background-color:#FCEFC9;">
				
	 			
				Nome: <strong><? echo $linha['nome']; ?></strong><br> 
				CPF: <strong> <? echo mascara($linha['cpf'],"###.###.###-##"); ?></strong><BR>
				Código Sizo: <strong> <? echo $linha['id']; ?></strong>
				
			</div>
			<div> 
			<a target="_blank" href="?ir=info_animal&id_animal=<? echo $linha['id']; ?>"  id="incluir" style="-moz-border-radius:0px 0px 7px 7px;
	-webkit-border-radius:0px 0px 7px 7px;
	 border-radius:0px 0px 7px 7px;" class="btn btn-flat btn-sm btn-info"><span id="incluir" class="btn-label  icon fa fa-search"></span> Mais Detalhes...</a>
						
			</div>
		
		
		</div><BR>
		
		<a id="vincular_animal" class="btn btn-success"><i class="fa fa-plus"> </i> Vincular</a>
		&nbsp&nbsp&nbsp<a id="cancelarEditar" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
		
		<?
				
			
		}else{
			
			echo '<BR><div  class="alert alert-danger alert-dark" style="width:300px; height:30px; padding-top: 4px;">
								<button type="button" class="close" data-dismiss="alert">×</button>
								Proprietário não encontrado...
							</div><BR><a id="cancelarEditar" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>';
		}
		
	
		}
	
}

function con_proprietario($param, $tipo, $min){
	if ($tipo=="con_proprietario_cpf_" or $tipo == "con_proprietario_animal_codigo" or $tipo == "con_proprietario_sizo"){
		$param = preg_replace( '#[^0-9]#', '',$param);	
	}
	
	if(strlen($param)>=$min){ 
		
		require "Conexao.class.php";
		require "funcoes.php";
		if($tipo=="con_proprietario_nome"){
			$param = trim($param);
			$sql = 'SELECT *  FROM proprietario WHERE nome like "%'.$param.'%" and deletado="0"';
		}
		if ($tipo=="con_proprietario_animal_codigo"){
			
			$param = preg_replace( '#[^0-9]#', '',$param);
			$sql = 'SELECT p.id, p.nome, p.cpf FROM proprietario P inner join animal A on a.Proprietario_cpf=p.cpf WHERE a.id="'.$param.'" and p.deletado="0" order by p.nome';
				
		}if ($tipo=="con_proprietario_cpf_"){
			$sql = 'SELECT *  FROM proprietario WHERE cpf="'.$param.'" and deletado="0" order by nome';
				
		}if ($tipo=="con_proprietario_sizo"){
			$sql = 'SELECT *  FROM proprietario WHERE id="'.$param.'" and deletado="0" order by nome';
				
		}
		
		$consulta = $conn->prepare($sql);
		$consulta ->execute();
		$count=  $consulta->fetch(PDO::FETCH_NUM);
		if($count){
			
			$consulta=null;
			?>
<br>			 
				
			<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover " id="<?php echo $tipo; ?>">
								<thead>
									<tr>
										<th>Nome</th>
										<th>CPF</th>
										<th>Código Sizo</th>
										<th>Ação</th>
									</tr>
								</thead>
								<tbody>
								<?
								
								
		
								   
								    foreach ($conn->query($sql) as $row) {
								    	$count++;
								    	
								    	?> <script>
								    	//EXCLUSÃO DE ANIMAL //
									    $("#<?php echo $tipo; ?>").on("click","#ui-bootbox-confirm<?php echo $row['id'];?>",function(){
									    	bootbox.confirm({
												message: "Voce tem certeza que deseja excluir o proprietário: <? echo $row['id'];?>",
												callback: function(result) {
												    if (result) {
													    $("#ajax-overlay").show();										 
												    	var info = 'id='+<? echo $row['id'];?>;
												    	$.ajax({
												    		type: "GET",
												    		url: "class/excluir.php?permissao=<?php echo $_GET['permissao_excluir']; ?>&tabela=proprietario&id_resp=<?php echo $_GET['id_resp'];?>&tipo=Proprietario&alvo=<? echo $row['id'];?>",
												    		data: info,
												    		dataType: "json",
												    		success: function(json){
														    		if(json.sucesso=="sim"){
															    		$("#ajax-overlay").hide();
															    		$('#pop_modal_success').modal('show');
														    			 $('#linha<? echo $row['id'];?>').fadeOut();
													    			 }else{
													    				 $("#ajax-overlay").hide();	
													    			 		if(json.tipo_erro=="permissao"){
																			$("#pop_modal_danger_permissao").modal('show');						
														    			 	}else{
														    			 		$("#pop_modal_danger").modal('show');	
															    			 	}
													    			 		
														    			 	
													    				
													    				 
														    			 }
												    			 }
												    		});
														
													}
												},
												className: "bootbox-sm"
											});
										});
								// FIM EXCLUSÃO ANIMAL //
								//EDITAR ANIMAL 	//
								
					    $("#<?php echo $tipo; ?>").on("click","#editar<? echo $row['id'];?>",function(){
							$('#ajax-overlay').show();
					        $('#div_vazia_<?php echo $tipo;?>').fadeIn('fast');
					    	$('#div_vazia_<?php echo $tipo;?>').load('edit_proprietario.php?id_proprietario=<?php echo $row['id'];?>&resp=<?php echo $id_atual;?>&permissao_alterar=<?php echo $_GET['permissao_alterar'];?>&idDiv=div_vazia_<?php echo $tipo;?>',function(){
					    		$('#ajax-overlay').hide();
					    		 $('html, body').animate({
				                        scrollTop: $("#div_vazia_<?php echo $tipo; ?>").offset().top-45
				                    }, 1000);
					    		$('#ajax-overlay').hide();
					    		
					    	});
					    });
				//FINAL EDITAR ANIMAL //
								
								
								    	 </script><?
								    	
								   
									
									echo "	
											<td>".$row['nome']."</td>
											<td>".mascara($row['cpf'],'###.###.###-##')."</td>	
											<td>".$row['id']."</td>     		
												";
								    	echo '
			
											<td><div  id="tooltips-demo">
						<a href="?ir=info_proprietario&id_proprietario='.$row['id'].'" class="btn btn-sm btn-primary btn-outline tooltip-info" data-toggle="tooltip" data-placement="top" data-original-title="Mais Informações"><i class="fa fa-info-circle"></i></a>
						<a id="editar"  href="?ir=info_proprietario&id_proprietario='.$row['id'].'&tab=edit_animal" class="btn btn-sm btn-warning btn-outline tooltip-warning" data-toggle="tooltip" data-placement="top" data-original-title="Editar Proprietário"><i class="fa fa-pencil"></i></a>
						<button id="ui-bootbox-confirm'.$row['id'].'"  class="btn btn-sm btn-danger btn-outline tooltip-danger" data-toggle="tooltip" data-placement="top" data-original-title="Excluir Proprietário" ><i class="fa fa-times"></i></button>
		
		</div></td>	
								    		  	
								    			</tr>
								    			
								    			';
								        
								    
								
								    }
								
								?>
								
									
									
								</tbody>
							</table>
							</div>
							</div>
							
							<div id="div_vazia_<?php echo $tipo;?>"> </div>
							
	<?
		}else{
			echo '<BR><div  class="alert alert-danger alert-dark "  style="width:300px; height:30px; padding-top: 4px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							Proprietário não encontrado...
						</div>';
		}
		?> <script>  function executaJs<?php echo $tipo; ?>() {
					
					$('#<?php echo $tipo; ?>').dataTable();
					$('#<?php echo $tipo; ?>_wrapper .table-caption').text('Proprietario');
					$('#<?php echo $tipo; ?>_wrapper .dataTables_filter input').attr('placeholder', 'Buscar...');
					 $('#tooltips-demo button').tooltip();
					    $('#tooltips-demo a').tooltip();
				
				
				
						
					}
							

				</script>  <?php 
		
	}else{
		echo ""; 
	}
}


function con_protocolo($param, $tipo, $min){
	if ($tipo=="con_protocolo_cpf" or $tipo == "con_protocolo_codigo" or $tipo == "con_protocolo_cnpj"){
		$param = preg_replace( '#[^0-9]#', '',$param);
	}

	if(strlen($param)>=$min){

		require "Conexao.class.php";
		require "funcoes.php";
		if($tipo=="con_protocolo_codigo"){
			
			$sql = 'SELECT p.id, p.solicitante, p.assunto, so.sigla as origem, sd.sigla as destino, p.situacao  FROM protocolo P 
		inner join setor so on p.origem = so.id
		inner join setor sd on p.destino = sd.id  
		
		WHERE P.id="'.$param.'" and p.deletado="0"';
		}
		if ($tipo=="con_protocolo_cpf"){
				
			
			$sql = 'SELECT p.id, p.solicitante, pa.descricao as assunto, so.sigla as origem, sd.sigla as destino, p.situacao  FROM protocolo P 
		inner join setor so on p.origem = so.id
		inner join setor sd on p.destino = sd.id
		inner join protocolo_assunto pa on p.assunto=pa.id
		WHERE p.cpf="'.$param.'" and p.deletado="0"';

		}if ($tipo=="con_protocolo_cnpj"){
			$sql = 'SELECT p.id, p.solicitante, pa.descricao as assunto, so.sigla as origem, sd.sigla as destino, p.situacao  FROM protocolo P 
		inner join setor so on p.origem = so.id
		inner join setor sd on p.destino = sd.id  
		inner join protocolo_assunto pa on p.assunto=pa.id
		WHERE p.cnpj="'.$param.'" and p.deletado="0"';

		}if ($tipo=="con_protocolo_solicitante"){
			$param = trim($param);
			$sql = 'SELECT p.id, p.solicitante, pa.descricao as assunto, so.sigla as origem, sd.sigla as destino, p.situacao  FROM protocolo P 
		inner join setor so on p.origem = so.id
		inner join setor sd on p.destino = sd.id  
		inner join protocolo_assunto pa on p.assunto=pa.id
		WHERE p.solicitante like "%'.$param.'%" and p.deletado="0"';

		}

		$consulta = $conn->prepare($sql);
		$consulta ->execute();
		$count=  $consulta->fetch(PDO::FETCH_NUM);
		if($count){
				
			$consulta=null;
			?>
<br>			 
				
			<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover " id="<?php echo $tipo; ?>">
								<thead>
									<tr>
										<th>Código</th>
										<th>Solicitante</th>
										<th>Assunto</th>
										<th>Origem</th>
										<th>Destino</th>
										<th>Situação</th>
										<th>Ações</th>
									</tr>
								</thead>
								<tbody>
								<?
								
								
		
								   
								    foreach ($conn->query($sql) as $row) {
								    	$count++;
								    	if($row['situacao']=="Em Progresso"){
								    		$cor_tipo = "info";
								    	}
								    	if($row['situacao']=="Pendente"){
								    		$cor_tipo = "warning";
								    	}
								    	if($row['situacao']=="Fechado"){
								    		$cor_tipo = "danger";
								    	}
								    	?> <script>
								    	//EXCLUSÃO DE Protocolo //
									    $("#<?php echo $tipo; ?>").on("click","#ui-bootbox-confirm<?php echo $row['id'];?>",function(){
									    	bootbox.confirm({
												message: "Voce tem certeza que deseja excluir o protocolo: <? echo $row['id'];?>",
												callback: function(result) {
												    if (result) {
													    $("#ajax-overlay").show();										 
												    	var info = 'id='+<? echo $row['id'];?>;
												    	$.ajax({
												    		type: "GET",
												    		url: "class/excluir.php?permissao=<?php echo $_GET['permissao_excluir']; ?>&tabela=protocolo&id_resp=<?php echo $_GET['id_resp'];?>&tipo=Protocolo&alvo=<? echo $row['id'];?>",
												    		data: info,
												    		dataType: "json",
												    		success: function(json){
														    		if(json.sucesso=="sim"){
															    		$("#ajax-overlay").hide();
															    		$('#pop_modal_success').modal('show');
														    			 $('#linha<? echo $row['id'];?>').fadeOut();
													    			 }else{
													    				 $("#ajax-overlay").hide();	
													    			 		if(json.tipo_erro=="permissao"){
																			$("#pop_modal_danger_permissao").modal('show');						
														    			 	}else{
														    			 		$("#pop_modal_danger").modal('show');	
															    			 	}
													    			 		
														    			 	
													    				
													    				 
														    			 }
												    			 }
												    		});
														
													}
												},
												className: "bootbox-sm"
											});
										});
								// FIM EXCLUSÃO ANIMAL //
								//EDITAR ANIMAL 	//
								
					   
				//FINAL EDITAR ANIMAL //
								
								
								    	 </script><?
								    	
								   
									
									echo "	
											<td>".$row['id']."</td>
											<td>".$row['solicitante']."</td>
											<td>".htmlentities($row['assunto'])."</td>
											<td>".$row['origem']."</td>
											<td>".$row['destino']."</td>
											<td><span class='label label-$cor_tipo badge-label'>".$row['situacao']."</span></td>   		
												";
								    	echo '
			
											<td><div  id="tooltips-demo">
						<a href="?ir=info_protocolo&id_protocolo='.$row['id'].'" class="btn btn-sm btn-primary btn-outline tooltip-info" data-toggle="tooltip" data-placement="top" data-original-title="Mais Informações"><i class="fa fa-info-circle"></i></a>
						<a id="editar"  href="?ir=info_protocolo&id_protocolo='.$row['id'].'&tab=edit_protocolo" class="btn btn-sm btn-warning btn-outline tooltip-warning" data-toggle="tooltip" data-placement="top" data-original-title="Editar Protocolo"><i class="fa fa-pencil"></i></a>
		<button id="ui-bootbox-confirm'.$row['id'].'"  class="btn btn-sm btn-danger btn-outline tooltip-danger" data-toggle="tooltip" data-placement="top" data-original-title="Excluir Protocolo" ><i class="fa fa-times"></i></button>
		
		</div></td>	
								    		  	
								    			</tr>
								    			
								    			';
								        
								    
								
								    }
								
								?>
								
									
									
								</tbody>
							</table>
							</div>
							</div>
							
							<div id="div_vazia_<?php echo $tipo;?>"> </div>
							
	<?
		}else{
			echo '<BR><div  class="alert alert-danger alert-dark "  style="width:300px; height:30px; padding-top: 4px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							Protocolo não encontrado...
						</div>';
		}
		?> <script>  function executaJs<?php echo $tipo; ?>() {
					
					$('#<?php echo $tipo; ?>').dataTable();
					$('#<?php echo $tipo; ?>_wrapper .table-caption').text('Protocolo');
					$('#<?php echo $tipo; ?>_wrapper .dataTables_filter input').attr('placeholder', 'Buscar...');
					 $('#tooltips-demo button').tooltip();
					    $('#tooltips-demo a').tooltip();
				
				
				
						
					}
							

				</script>  <?php 
		
	}else{
		echo ""; 
	}
}


function con_animal($param, $tipo, $min){
	if ($tipo=="con_animal_proprietario_cpf" or $tipo == "con_animal_codigo" or $tipo == "con_animal_sizo"){
		$param = preg_replace( '#[^0-9]#', '',$param);
	}
	if(strlen($param)>=$min){

		require "Conexao.class.php";
		require "funcoes.php";
		if($tipo=="con_animal_nome"){
			$param = trim($param);
			$sql = 'SELECT id, codigo, nome, Proprietario_cpf  FROM animal WHERE nome like "%'.$param.'%" and deletado="0"';
		}
		if ($tipo=="con_animal_proprietario_cpf"){
			$param = preg_replace( '#[^0-9]#', '',$param);
			$sql = 'SELECT id, codigo, nome, Proprietario_cpf  FROM animal WHERE Proprietario_cpf="'.$param.'" and deletado="0" order by nome';

		}if ($tipo=="con_animal_codigo"){
			$sql = 'SELECT id, codigo, nome, Proprietario_cpf  FROM animal WHERE codigo="'.$param.'" and deletado="0" order by nome';

		}if ($tipo=="con_animal_sizo"){
			$sql = 'SELECT id, codigo, nome, Proprietario_cpf  FROM animal WHERE id="'.$param.'" and deletado="0" order by nome';

		}

		$consulta = $conn->prepare($sql);
		$consulta ->execute();
		$count=  $consulta->fetch(PDO::FETCH_NUM);
		if($count){
				
			$consulta=null;
			?>
<br>			 
				
			<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover " id="<?php echo $tipo; ?>">
								<thead>
									<tr>
										<th>Foto</th>
										<th>Nome</th>
										<th>Código Sizo</th>
										<th>Código RGA</th>
										<th>CPF Proprietário</th>
										<th>Ação</th>
									</tr>
								</thead>
								<tbody>
								<?
								
								
		
								   
								    foreach ($conn->query($sql) as $row) {
								    	$count++;
								    	
								    	?> <script>
								    	//EXCLUSÃO DE ANIMAL //
									    $("#<?php echo $tipo; ?>").on("click","#ui-bootbox-confirm<?php echo $row['id'];?>",function(){
									    	bootbox.confirm({
												message: "Voce tem certeza que deseja excluir o animal: <? echo $row['id'];?>",
												callback: function(result) {
												    if (result) {
													    $("#ajax-overlay").show();										 
												    	var info = 'id='+<? echo $row['id'];?>;
												    	$.ajax({
												    		type: "GET",
												    		url: "class/excluir.php?permissao=<?php echo $_GET['permissao_excluir']; ?>&tabela=animal&id_resp=<?php echo $_GET['id_resp'];?>&tipo=Animal&alvo=<? echo $row['id'];?>",
												    		data: info,
												    		dataType: "json",
												    		success: function(json){
														    		if(json.sucesso=="sim"){
															    		$("#ajax-overlay").hide();
															    		$('#pop_modal_success').modal('show');
														    			 $('#linha<? echo $row['id'];?>').fadeOut();
													    			 }else{
													    				 $("#ajax-overlay").hide();	
													    			 		if(json.tipo_erro=="permissao"){
																			$("#pop_modal_danger_permissao").modal('show');						
														    			 	}else{
														    			 		$("#pop_modal_danger").modal('show');	
															    			 	}
													    			 		
														    			 	
													    				
													    				 
														    			 }
												    			 }
												    		});
														
													}
												},
												className: "bootbox-sm"
											});
										});
								// FIM EXCLUSÃO ANIMAL //
								//EDITAR ANIMAL 	//
								
					    $("#<?php echo $tipo; ?>").on("click","#editar<? echo $row['id'];?>",function(){
							$('#ajax-overlay').show();
					        $('#div_vazia_<?php echo $tipo;?>').fadeIn('fast');
					    	$('#div_vazia_<?php echo $tipo;?>').load('edit_animal.php?animal=<?php echo $row['id'];?>&resp=<?php echo $id_atual;?>&permissao_alterar=<?php echo $_GET['permissao_alterar'];?>&idDiv=div_vazia_<?php echo $tipo;?>',function(){
					    		$('#ajax-overlay').hide();
					    		 $('html, body').animate({
				                        scrollTop: $("#div_vazia_<?php echo $tipo; ?>").offset().top-45
				                    }, 1000);
					    		$('#ajax-overlay').hide();
					    		
					    	});
					    });
				//FINAL EDITAR ANIMAL //
								
								
								    	 </script><?
								    	$fotos_animal_p = glob("../view/img/animal/".$row['id']."/*p.jpg");
								    	$fotos_animal_g = glob("../view/img/animal/".$row['id']."/*g.jpg");
								    	
								   
									
									if (file_exists($fotos_animal_p[0])) {
									  $imagem=substr($fotos_animal_p[0], 3);
									  $imagem2=substr($fotos_animal_g[0], 3);
									} else {
									   $imagem="view/img/noimg.png";
									   $imagem2="view/img/noimg.png";
									}
								    	echo '	<tr id="linha'.$row['id'].'">';
								    	echo "
											<td align=\"center\"><a href=\"$imagem2\" data-fancybox-group=\"gallery\" class=\"dropdown-toggle user-menu fancybox\" data-toggle=\"dropdown\">
									<img class=\"rounded\" width=\"30px\" src=\"$imagem\" alt=\"\">
								</td>	
											<td>".$row['nome']."</td>	
											<td>".$row['id']."</td>
        									<td>".$row['codigo']."</td>        		
											<td>".mascara($row['Proprietario_cpf'],'###.###.###-##')."</td>	";
								    	echo '
			
											<td><div  id="tooltips-demo">
						<a href="?ir=info_animal&id_animal='.$row['id'].'" class="btn btn-sm btn-primary btn-outline tooltip-info" data-toggle="tooltip" data-placement="top" data-original-title="Mais Informações"><i class="fa fa-info-circle"></i></a>
						<a id="editar"  href="?ir=info_animal&id_animal='.$row['id'].'&tab=edit_animal" class="btn btn-sm btn-warning btn-outline tooltip-warning" data-toggle="tooltip" data-placement="top" data-original-title="Editar Animal"><i class="fa fa-pencil"></i></a>
						<button id="ui-bootbox-confirm'.$row['id'].'"  class="btn btn-sm btn-danger btn-outline tooltip-danger" data-toggle="tooltip" data-placement="top" data-original-title="Excluir Animal" ><i class="fa fa-times"></i></button>
		
		</div></td>	
								    		  	
								    			</tr>
								    			
								    			';
								        
								    
								
								    }
								
								?>
								
									
									
								</tbody>
							</table>
							</div>
							</div>
							
							<div id="div_vazia_<?php echo $tipo;?>"> </div>
							
	<?
		}else{
			echo '<BR><div  class="alert alert-danger alert-dark "  style="width:300px; height:30px; padding-top: 4px;">
							<button type="button" class="close" data-dismiss="alert">×</button>
							Animal não encontrado...
						</div>';
		}
		?> <script>  function executaJs<?php echo $tipo; ?>() {
					
					$('#<?php echo $tipo; ?>').dataTable();
					$('#<?php echo $tipo; ?>_wrapper .table-caption').text('Animal');
					$('#<?php echo $tipo; ?>_wrapper .dataTables_filter input').attr('placeholder', 'Buscar...');
					 $('#tooltips-demo button').tooltip();
					    $('#tooltips-demo a').tooltip();
				
				
				
						
					}
							

				</script>  <?php 
		
	}else{
		echo ""; 
	}
}




	
?>





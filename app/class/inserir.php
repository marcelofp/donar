<?php
sleep(2);
header('Cache-Control: no-cache, must-revalidate');
header('Content-Type: application/json; charset=utf-8');
$funcao = $_GET["funcao"];
function retorno($sucesso, $tipo_erro, $id){
	$arr = array();
	$arr['sucesso'] = $sucesso;
	$arr['tipo_erro'] = $tipo_erro;
	$arr['id'] = $id;
	echo json_encode($arr);

}

function cad_pessoa(){
	require "Conexao.class.php";
	$sql = $conn->prepare("SELECT * from pessoa where email='".$_POST['email']."' and deletado=0");
	$sql->execute();
	$rows = $sql->fetch(PDO::FETCH_NUM);
	if($rows > 0) {
		
		retorno("nao", "email_existe", "", "");
			
	}else{
	
		$result = $conn->prepare("INSERT INTO pessoa (nome, telefone, email, senha, data_cadastro) VALUES (:nome, :telefone, :email, :senha, :data_cadastro)");
		$result->bindValue(':nome', $_POST['nome']);
		$result->bindValue(':telefone', $_POST['telefone']);
		$result->bindValue(':email', $_POST['email']);
		$result->bindValue(':senha', $_POST['senha']);
		$data = date("Y-m-d H:i:s");
		$result->bindValue(':data_cadastro', $data);
	
		if($result->execute()) {
			$sql_consulta = 'SELECT * from pessoa where email="'.$_POST['email'].'"';
			foreach ($conn->query($sql_consulta) as $row) {
				$idpessoa = $row['idpessoa'];
				$pessoa_novo = $row['email'];
			}
				retorno("sim","",$idpessoa);
				
					
		}
		else{
				retorno("nao", "erro_banco", "");
	
		}
	
	}
	
}

function cad_doacao(){
	require "Conexao.class.php";
		$result = $conn->prepare("INSERT INTO doar (tipo, titulo, descricao, data_cadastro, idpessoa) VALUES (:tipo, :titulo, :descricao, :data_cadastro, :idpessoa)");
		$result->bindValue(':tipo', $_POST['tipo']);
		$result->bindValue(':titulo', $_POST['titulo']);
		$result->bindValue(':descricao', $_POST['descricao']);
		$data = date("Y-m-d H:i:s");
		$result->bindValue(':data_cadastro', $data);
		$result->bindValue(':idpessoa', $_GET['idpessoa']);		

		if($result->execute()) {
			$sql_consulta = 'select * from doar order by iddoar desc LIMIT 1';
			foreach ($conn->query($sql_consulta) as $row) {
				$iddoar = $row['iddoar'];
			}
			retorno("sim","",$iddoar);

				
		}
		else{
			retorno("nao", "erro_banco", "");

		}

	

}























function auditoria($id_responsavel, $tipo_auditoria, $tipo_alvo, $alvo, $ip_interno, $nome_pc){
	require "Conexao.class.php";	
	$result = $conn->prepare("INSERT INTO auditoria (id_usuario, tipo_auditoria,tipo_alvo,alvo,ip_interno,nome_pc,data_cadastro) VALUES (:id_usuario, :tipo_auditoria,:tipo_alvo,:alvo,:ip_interno,:nome_pc,:data_cadastro)");
	$result->bindValue(':id_usuario', $id_responsavel);
	$result->bindValue(':tipo_auditoria',  $tipo_auditoria);
	$result->bindValue(':tipo_alvo', $tipo_alvo);
	$result->bindValue(':alvo', $alvo);
	$result->bindValue(':ip_interno', $ip_interno);
	$result->bindValue(':nome_pc', $nome_pc);
	$data = date("Y-m-d H:i:s");
	$result->bindValue(':data_cadastro', $data);
	$result->execute();
	
}
function cad_usuario($id_responsavel,$nome,$cpf,$usuario,$senha,$telefone,$email,$Cargo_id,$Setor_id){
	require "Conexao.class.php";	
	$sql = $conn->prepare("SELECT * from usuario where usuario = '$usuario' or email='$email' and deletado=0"); 
	$sql->execute();
	$rows = $sql->fetch(PDO::FETCH_NUM);
	if($rows > 0) {
		$arr = array(); 
		$arr['sucesso'] = "nao";
		$arr['id'] = "";
		$arr['erro'] = 101;
		echo json_encode($arr);
		
	}else{
	
		$result = $conn->prepare("INSERT INTO usuario (nome, cpf, usuario, senha, telefone, email, Cargo_id, Setor_id, data_cadastro, cadastrado_por) VALUES (:nome, :cpf, :usuario, :senha, :telefone, :email, :Cargo_id, :Setor_id, :data_cadastro, :cadastrado_por)");
		$result->bindValue(':nome', $nome);
		$result->bindValue(':cpf',  preg_replace( '#[^0-9]#', '', $cpf ));
		$result->bindValue(':usuario', $usuario);
		$result->bindValue(':senha', $senha);
		$result->bindValue(':telefone', $telefone);
		$result->bindValue(':email', $email);
		$result->bindValue(':Setor_id', $Setor_id);
		$result->bindValue(':Cargo_id', $Cargo_id);
		$data = date("Y-m-d H:i:s");
		$result->bindValue(':data_cadastro', $data);
		$result->bindValue(':cadastrado_por', $_GET['resp_usuario']);
		
		if($result->execute()) {
			
			$sql_consulta = 'SELECT * from usuario where usuario="'.$usuario.'"';
			foreach ($conn->query($sql_consulta) as $row) {
				$id = $row['id']; 
				$usuario_novo = $row['usuario'];}
				auditoria($id_responsavel,"Inclusao","Usuario",$usuario_novo,$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']));
					
				$arr = array(); 
				$arr['sucesso'] = "sim";
				$arr['id'] = $id;
				$arr['erro'] = 100;
				echo json_encode($arr);
			
		}
		else{
				$arr = array(); 
				$arr['sucesso'] = "nao";
				$arr['id'] = "";
				$arr['erro'] = 999;
				echo json_encode($arr);
		
		}
	
	}
	
	
}

function cad_animal(){
	require "Conexao.class.php";
	$sql = $conn->prepare("SELECT * from animal where codigo = '".$_POST['identificacao']."' and deletado=0");
	$sql->execute();
	$rows = $sql->fetch(PDO::FETCH_NUM);
	if($rows > 0) {
		$arr = array();
		$arr['sucesso'] = "nao";
		$arr['id'] = "";
		$arr['erro'] = 201;
		echo json_encode($arr);
	
	}else{
		//Tratamento de Variáveis inteiras.
		if($_POST['identificacao']=="")$_POST['identificacao']=NULL;
		if($_POST['raca']=="")$_POST['raca']=NULL;
		if($_POST['base_nasc']=="")$_POST['base_nasc']=NULL;
		if($_POST['cpf']=="")$cpf=NULL;else $cpf=preg_replace( '#[^0-9]#', '',$_POST['cpf']);
		$result = $conn->prepare("INSERT INTO animal 
				(codigo, nome, Proprietario_cpf, idade, observacao, Cor_id, Especie_id, cadastrado_por, data_cadastro, 
				Raca_id, localizacao, pelagem_longo, pelagem_sedoso, pelagem_liso, pelagem_aspero, pelagem_ondulado, pelagem_denso, origem, endereco, bairro, sexo, castrado) 
				VALUES 
				(:codigo, :nome, :Proprietario_cpf, :idade, :observacao, :Cor_id, :Especie_id, :cadastrado_por, 
				:data_cadastro, :Raca_id, :localizacao, :pelagem_longo, :pelagem_sedoso, :pelagem_liso, :pelagem_aspero, :pelagem_ondulado, :pelagem_denso, :origem, :endereco, :bairro, :sexo, :castrado)");
		
		$result->bindValue(':codigo', $_POST['identificacao']);
		$result->bindValue(':nome', $_POST['nome']);
		$result->bindValue(':Proprietario_cpf', $cpf);
		$result->bindValue(':idade', $_POST['base_nasc']);
		$result->bindValue(':observacao', $_POST['observacao']);
		$result->bindValue(':Cor_id', $_POST['cor']);
		$result->bindValue(':Especie_id', $_POST['especie']);
		$result->bindValue(':cadastrado_por', $_GET['resp_usuario']);
		$data = date("Y-m-d H:i:s");
		$result->bindValue(':data_cadastro', $data);
		$result->bindValue(':Raca_id', $_POST['raca']);
		$result->bindValue(':localizacao', $_POST['localizacao']);
		$result->bindValue(':pelagem_longo', $_POST['pelagem_longo']);
		$result->bindValue(':pelagem_sedoso', $_POST['pelagem_sedoso']);
		$result->bindValue(':pelagem_liso', $_POST['pelagem_liso']);
		$result->bindValue(':pelagem_aspero', $_POST['pelagem_aspero']);
		$result->bindValue(':pelagem_ondulado', $_POST['pelagem_ondulado']);
		$result->bindValue(':pelagem_denso', $_POST['pelagem_denso']);
		$result->bindValue(':origem', $_POST['origem']);
		$result->bindValue(':endereco', $_POST['endereco']);
		$result->bindValue(':bairro', $_POST['bairro']);
		$result->bindValue(':sexo', $_POST['sexo']);
		$result->bindValue(':castrado', $_POST['castrado']);
		
		if($result->execute()) {
			
				
			$sql_consulta = 'select * from animal order by id desc LIMIT 1';
			foreach ($conn->query($sql_consulta) as $row) {
				$animal_novo = $row['id'];}
				auditoria($_GET['resp_id'],"Inclusao","Animal",$animal_novo,$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']));
				$arr = array();
				$arr['sucesso'] = "sim";
				$arr['id'] = $animal_novo;
				$arr['erro'] = 100;
				echo json_encode($arr);
			
		}
		else{
			$arr = array();
			$arr['sucesso'] = "nao";
			$arr['id'] = "";
			$arr['erro'] = 999;
			echo json_encode($arr);
	
		}
	
	}
}

function cad_proprietario(){
	require "Conexao.class.php";	
	$cpf = preg_replace( '#[^0-9]#', '', $_POST['cpf'] );
	$sql = $conn->prepare("SELECT * from proprietario where cpf = '{$cpf}' and deletado=0"); 
	$sql->execute();
	$rows = $sql->fetch(PDO::FETCH_NUM);
	if($rows > 0) {
		$arr = array(); 
		$arr['sucesso'] = "nao";
		$arr['id'] = "";
		$arr['erro'] = 101;
		echo json_encode($arr);
		
	}else{
		$result = $conn->prepare("
		INSERT INTO proprietario(
		nome, 
		cpf, 
		sexo, 
		rg, 
		observacao, 
		dt_nascimento, 
		cep,
		tipo_logradouro, 
		logradouro, 
		numero, 
		complemento, 
		bairro, 
		cidade, 
		estado, 
		contato1, 
		contato2, 
		data_cadastro,
		cadastrado_por) 
		VALUES (
		:nome, 
		:cpf, 
		:sexo, 
		:rg, 
		:observacao, 
		:dt_nascimento, 
		:cep, 
		:tipo_logradouro, 
		:logradouro, 
		:numero, 
		:complemento, 
		:bairro, 
		:cidade, 
		:estado, 
		:contato1, 
		:contato2, 
		:data_cadastro, 
		:cadastrado_por)");
		$result->bindValue(':nome', $_POST['nome']);
		$result->bindValue(':cpf',  $cpf);
		$result->bindValue(':sexo',$_POST['sexo']);
		$result->bindValue(':rg', $_POST['rg']);
		$result->bindValue(':observacao', $_POST['observacao']);
		$dt_nasc = explode("/", $_POST['dt_nascimento']);
		$dt_nasc = $dt_nasc[2]."-".$dt_nasc[1]."-".$dt_nasc[0];
		$result->bindValue(':dt_nascimento', $dt_nasc);
		$result->bindValue(':cep',$_POST['cep']);
		$result->bindValue(':tipo_logradouro', $_POST['tipo_logradouro']);
		$result->bindValue(':logradouro', $_POST['logradouro']);
		$result->bindValue(':numero', $_POST['numero']);
		$result->bindValue(':complemento', $_POST['complemento']);
		$result->bindValue(':bairro', $_POST['bairro']);
		$result->bindValue(':cidade',$_POST['cidade']);
		$result->bindValue(':estado', $_POST['estado']);
		$result->bindValue(':contato1', $_POST['contato1']);
		$result->bindValue(':contato2', $_POST['contato2']);
		$data = date("Y-m-d H:i:s");
		$result->bindValue(':data_cadastro', $data);
		$result->bindValue(':cadastrado_por', $_GET['resp']);
		
		if($result->execute()) {
			
			$sql_consulta = 'SELECT * from proprietario where cpf="'.preg_replace( '#[^0-9]#', '', $_POST['cpf'] ).'"';
			foreach ($conn->query($sql_consulta) as $row) {
				$id = $row['id']; 
				$proprietario_novo = $row['nome'];}
				auditoria($_GET['resp_id'],"Inclusao","Proprietario",$id,$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']));
					
				$arr = array(); 
				$arr['sucesso'] = "sim";
				$arr['id'] = $id;
				$arr['erro'] = 100;
				echo json_encode($arr);
			
		}
		else{
				$arr = array(); 
				$arr['sucesso'] = "nao";
				$arr['id'] = "";
				$arr['erro'] = 999;
				echo json_encode($arr);
		
		}
	
	}
	
	
}

function cad_setor(){
	require "Conexao.class.php";
	$sql = $conn->prepare("SELECT * from setor where descricao = '{$_POST['descricao']}' or sigla='{$_POST['sigla']}' and deletado=0");
	$sql->execute();
	$rows = $sql->fetch(PDO::FETCH_NUM);
	if($rows > 0) {
		$arr = array();
		$arr['sucesso'] = "nao";
		$arr['id'] = "";
		$arr['erro'] = 101;
		echo json_encode($arr);

	}else{

		$result = $conn->prepare("INSERT INTO setor (descricao, sigla, data_cadastro, cadastrado_por) VALUES (:descricao, :sigla, :data_cadastro, :cadastrado_por)");
		$result->bindValue(':descricao', $_POST['descricao']);
		$result->bindValue(':sigla',  $_POST['sigla']);
		$data = date("Y-m-d H:i:s");
		$result->bindValue(':data_cadastro', $data);
		$result->bindValue(':cadastrado_por', $_GET['usuario_resp']);

		if($result->execute()) {
				
			$sql_consulta = 'SELECT * from setor where sigla="'.$_POST['sigla'].'"';
			foreach ($conn->query($sql_consulta) as $row) {
				$id = $row['id'];}
				auditoria($_GET['id_resp'],"Inclusao","Setor",$id,$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']));
					
				$arr = array();
				$arr['sucesso'] = "sim";
				$arr['id'] = $id;
				$arr['erro'] = 100;
				echo json_encode($arr);
					
		}
		else{
			$arr = array();
			$arr['sucesso'] = "nao";
			$arr['id'] = "";
			$arr['erro'] = 999;
			echo json_encode($arr);

		}

	}


}


function cad_cargo(){
	require "Conexao.class.php";
	$sql = $conn->prepare("SELECT * from cargo where descricao = '{$_POST['descricao']}'  and deletado=0");
	$sql->execute();
	$rows = $sql->fetch(PDO::FETCH_NUM);
	if($rows > 0) {
		$arr = array();
		$arr['sucesso'] = "nao";
		$arr['id'] = "";
		$arr['erro'] = 101;
		echo json_encode($arr);

	}else{

		$result = $conn->prepare("INSERT INTO cargo (descricao, data_cadastro, cadastrado_por) VALUES (:descricao, :data_cadastro, :cadastrado_por)");
		$result->bindValue(':descricao', $_POST['descricao']);
		$data = date("Y-m-d H:i:s");
		$result->bindValue(':data_cadastro', $data);
		$result->bindValue(':cadastrado_por', $_GET['usuario_resp']);

		if($result->execute()) {
			$sql_consulta = 'select * from setor order by id desc LIMIT 1';
			
			foreach ($conn->query($sql_consulta) as $row) {
				$id = $row['id'];}
				auditoria($_GET['id_resp'],"Inclusao","Cargo",$id,$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']));
					
				$arr = array();
				$arr['sucesso'] = "sim";
				$arr['id'] = $id;
				$arr['erro'] = 100;
				echo json_encode($arr);
					
		}
		else{
			$arr = array();
			$arr['sucesso'] = "nao";
			$arr['id'] = "";
			$arr['erro'] = 999;
			echo json_encode($arr);

		}

	}


}

function cad_estoque(){
	require "Conexao.class.php";
	$sql = $conn->prepare("SELECT * from estoque where descricao = '{$_POST['descricao']}' and deletado=0");
	$sql->execute();
	$rows = $sql->fetch(PDO::FETCH_NUM);
	if($rows > 0) {
		$arr = array();
		$arr['sucesso'] = "nao";
		$arr['id'] = "";
		$arr['erro'] = 101;
		echo json_encode($arr);

	}else{

		$result = $conn->prepare("INSERT INTO estoque (descricao, unidade_medida, quantidade, cadastrado_por, data_cadastro, categoria) 
				VALUES (:descricao, :unidade_medida, :quantidade, :cadastrado_por, :data_cadastro, :categoria)");
		$result->bindParam(':descricao', $_POST['descricao']);
		$result->bindParam(':unidade_medida', $_POST['unidade_medida']);
		$result->bindParam(':quantidade', $_POST['quantidade']);
		$result->bindParam(':categoria', $_POST['categoria']);
		$data = date("Y-m-d H:i:s");
		$result->bindValue(':data_cadastro', $data);
		$result->bindValue(':cadastrado_por', $_GET['usuario_resp']);

		if($result->execute()) {

			$sql_consulta = 'select * from estoque order by id desc LIMIT 1';
			foreach ($conn->query($sql_consulta) as $row) {
				$id = $row['id'];}
				auditoria($_GET['id_resp'],"Inclusao","Item Estoque",$id,$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']));
					
				$arr = array();
				$arr['sucesso'] = "sim";
				$arr['id'] = $id;
				$arr['erro'] = 100;
				echo json_encode($arr);
					
		}
		else{
			$arr = array();
			$arr['sucesso'] = "nao";
			$arr['id'] = "";
			$arr['erro'] = 999;
			echo json_encode($arr);

		}

	}


}

function cad_estoque_movimento(){
	require "Conexao.class.php";
	$result = $conn->prepare("INSERT INTO estoque_movimento (Produto_id, tipo_movimento, quantidade, data_cadastro, Usuario_id) VALUES (:Produto_id, :tipo_movimento, :quantidade, :data_cadastro, :Usuario_id)");
		$result->bindValue(':Produto_id', $_GET['id_estoque']);
		$result->bindValue(':tipo_movimento',  $_GET['tipo']);
		$result->bindValue(':quantidade',  $_POST['quantidade']);
		$data = explode("/", $_POST['data']);
		$data = $data[2]."-".$data[1]."-".$data[0];
		$result->bindValue(':data_cadastro', $data);
		$result->bindValue(':Usuario_id', $_GET['id_resp']);
	if($result->execute()){
		if($_GET['tipo']=="Entrada"){
			$sql = $conn->prepare("update estoque set quantidade=quantidade+{$_POST['quantidade']} where id='{$_GET['id_estoque']}'");
			
		}
		else{
			$sql = $conn->prepare("update estoque set quantidade=quantidade-{$_POST['quantidade']} where id='{$_GET['id_estoque']}'");
			
		}
		$sql->execute();
		auditoria($_GET['id_resp'],"Movimento",$_GET['tipo']." Estoque",$_GET['id_estoque'],$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']),"");
		retorno("sim", "nenhum", "", "");
	}else{
		retorno("nao", "banco", "", "");
	}

}

function cad_protocolo(){
	require "Conexao.class.php";
	$cpf=preg_replace( '#[^0-9]#', '',$_POST['cpf']);
	$cnpj=preg_replace( '#[^0-9]#', '',$_POST['cnpj']);
	$result = $conn->prepare("INSERT INTO protocolo (data_emissao, solicitante, tipo_solicitante, cpf, cnpj, assunto, origem, destino, setor_atual, descricao, data_cadastro, Usuario_id) 
			VALUES (:data_emissao, :solicitante, :tipo_solicitante, :cpf, :cnpj, :assunto, :origem, :destino, :setor_atual, :descricao, :data_cadastro, :Usuario_id)");
	$data_emissao = explode("/", $_POST['data_emissao']);
	$data_emissao = $data_emissao[2]."-".$data_emissao[1]."-".$data_emissao[0];
	$result->bindValue(':data_emissao', $data_emissao);
	$result->bindValue(':solicitante', $_POST['solicitante']);
	$result->bindValue(':tipo_solicitante',  $_POST['tipo_solicitante']);
	$result->bindValue(':cpf', $cpf);
	$result->bindValue(':cnpj', $cnpj);
	$result->bindValue(':assunto', $_POST['assunto']);
	$result->bindValue(':origem', $_POST['setor_origem']);
	$result->bindValue(':destino', $_POST['setor_destino']);
	$result->bindValue(':setor_atual', $_POST['setor_origem']);
	$result->bindValue(':descricao', $_POST['descricao']);
	$data = date("Y-m-d H:i:s");
	$result->bindValue(':data_cadastro', $data);
	$result->bindValue(':Usuario_id', $_GET['id_resp']);
	if($result->execute()){
		$sql_consulta = 'select * from protocolo order by id desc LIMIT 1';
		foreach ($conn->query($sql_consulta) as $row) {
			$id = $row['id'];}
		auditoria($_GET['id_resp'],"Cadastro","Protocolo",$id,$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']),"");
		retorno("sim", "nenhum", $id, "");
	}else{
		retorno("nao", "banco", "", "");
	}

}

function cad_protocolo_tramitacao(){
	require "Conexao.class.php";
	$sql_consulta_passo = "select * from protocolo_tramites where Protocolo_id='{$_GET['id_protocolo']}' and deletado='0' order by passo desc LIMIT 1";
	foreach ($conn->query($sql_consulta_passo) as $row) {
		$passo_antigo = $row['passo'];
	}
	$passo = $passo_antigo+1;
	$result = $conn->prepare("INSERT INTO protocolo_tramites (Protocolo_id, descricao, passo, Setor_id_Origem, Setor_id_Destino, Usuario_Enviou, data_emissao, data_cadastro)
			VALUES (:Protocolo_id, :descricao, :passo, :Setor_id_Origem, :Setor_id_Destino, :Usuario_Enviou, :data_emissao, :data_cadastro)");
	$data_emissao = explode("/", $_POST['data_emissao']);
	$data_emissao = $data_emissao[2]."-".$data_emissao[1]."-".$data_emissao[0];
	$result->bindValue(':data_emissao', $data_emissao);
	$result->bindValue(':Protocolo_id', $_GET['id_protocolo']);
	$result->bindValue(':descricao',  $_POST['descricao']);
	$result->bindValue(':passo',  $passo);
	$result->bindValue(':Setor_id_Origem', $_GET['origem']);
	$result->bindValue(':Setor_id_Destino', $_POST['setor_destino']);
	$result->bindValue(':Usuario_Enviou', $_GET['id_resp']);
	$data = date("Y-m-d H:i:s");
	$result->bindValue(':data_cadastro', $data);
	if($result->execute()){
		$sqlsss = 'update protocolo set destino="'.$_POST['setor_destino'].'", situacao="Pendente" where id='.$_GET['id_protocolo'];
		$conn->query($sqlsss);
			auditoria($_GET['id_resp'],"Cadastro","Tramitação de Protocolo",$_GET['id_protocolo'],$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']),"");
			retorno("sim", "nenhum", $_GET['id_protocolo'], "");
	}else{
		retorno("nao", "banco", "", "");
	}

}

function cad_protocolo_receber(){
	require "Conexao.class.php";
	$sql_consulta_passo = "select * from protocolo_tramites where Protocolo_id='{$_GET['id_protocolo']}' and deletado='0' order by passo desc LIMIT 1";
	foreach ($conn->query($sql_consulta_passo) as $row) {
		$passo_antigo = $row['passo'];
	}
	$passo = $passo_antigo+1;
	$result = $conn->prepare("INSERT INTO protocolo_tramites (Protocolo_id, descricao, passo, situacao, Setor_id_Origem, Setor_id_Destino, Usuario_Recebeu, data_cadastro)
			VALUES (:Protocolo_id, :descricao, :passo, :situacao, :Setor_id_Origem, :Setor_id_Destino,  :Usuario_Recebeu, :data_cadastro)");
	$data_emissao = explode("/", $_POST['data_emissao']);
	$data_emissao = $data_emissao[2]."-".$data_emissao[1]."-".$data_emissao[0];
	$result->bindValue(':Protocolo_id', $_GET['id_protocolo']);
	$result->bindValue(':descricao',  "Protocolo recebido. Passo automático!");
	$result->bindValue(':passo',  $passo);
	$result->bindValue(':situacao',  "Em Progresso");
	$result->bindValue(':Setor_id_Origem', $_GET['origem']);
	$result->bindValue(':Setor_id_Destino', $_GET['origem']);
	$result->bindValue(':Usuario_Recebeu', $_GET['id_resp']);
	$data = date("Y-m-d H:i:s");
	$result->bindValue(':data_cadastro', $data);
	if($result->execute()){
	$sqlsss = 'update protocolo set setor_atual="'.$_GET['origem'].'", situacao="Em Progresso" where id='.$_GET['id_protocolo'];
	$conn->query($sqlsss);
		auditoria($_GET['id_resp'],"Cadastro","Recebeu Protocolo",$_GET['id_protocolo'],$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']),"");
		retorno("sim", "nenhum", $_GET['id_protocolo'], "");
	}else{
		retorno("nao", "banco", "", "");
	}

}

function cad_protocolo_fechar(){
	require "Conexao.class.php";
	$sql_consulta_passo = "select * from protocolo_tramites where Protocolo_id='{$_GET['id_protocolo']}' and deletado='0' order by passo desc LIMIT 1";
	foreach ($conn->query($sql_consulta_passo) as $row) {
		$passo_antigo = $row['passo'];
	}
	$passo = $passo_antigo+1;
	$result = $conn->prepare("INSERT INTO protocolo_tramites (Protocolo_id, descricao, passo, situacao, Setor_id_Origem, Setor_id_Destino, Usuario_Recebeu, data_cadastro)
			VALUES (:Protocolo_id, :descricao, :passo, :situacao, :Setor_id_Origem, :Setor_id_Destino,  :Usuario_Recebeu, :data_cadastro)");
	$data_emissao = explode("/", $_POST['data_emissao']);
	$data_emissao = $data_emissao[2]."-".$data_emissao[1]."-".$data_emissao[0];
	$result->bindValue(':Protocolo_id', $_GET['id_protocolo']);
	$result->bindValue(':descricao',  "Protocolo Fechado. Passo automático!");
	$result->bindValue(':passo',  $passo);
	$result->bindValue(':situacao',  "Fechado");
	$result->bindValue(':Setor_id_Origem', $_GET['origem']);
	$result->bindValue(':Setor_id_Destino', $_GET['origem']);
	$result->bindValue(':Usuario_Recebeu', $_GET['id_resp']);
	$data = date("Y-m-d H:i:s");
	$result->bindValue(':data_cadastro', $data);
	if($result->execute()){
		$sqlsss = 'update protocolo set setor_atual="'.$_GET['origem'].'", situacao="Fechado" where id='.$_GET['id_protocolo'];
		$conn->query($sqlsss);
		auditoria($_GET['id_resp'],"Cadastro","Fechou Protocolo",$_GET['id_protocolo'],$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']),"");
		retorno("sim", "nenhum", $_GET['id_protocolo'], "");
	}else{
		retorno("nao", "banco", "", "");
	}

}

function cad_protocolo_reabrir(){
	require "Conexao.class.php";
	$sql_consulta_passo = "select * from protocolo_tramites where Protocolo_id='{$_GET['id_protocolo']}' and deletado='0' order by passo desc LIMIT 1";
	foreach ($conn->query($sql_consulta_passo) as $row) {
		$passo_antigo = $row['passo'];
	}
	$passo = $passo_antigo+1;
	$result = $conn->prepare("INSERT INTO protocolo_tramites (Protocolo_id, descricao, passo, situacao, Setor_id_Origem, Setor_id_Destino, Usuario_Recebeu, data_cadastro)
			VALUES (:Protocolo_id, :descricao, :passo, :situacao, :Setor_id_Origem, :Setor_id_Destino,  :Usuario_Recebeu, :data_cadastro)");
	$data_emissao = explode("/", $_POST['data_emissao']);
	$data_emissao = $data_emissao[2]."-".$data_emissao[1]."-".$data_emissao[0];
	$result->bindValue(':Protocolo_id', $_GET['id_protocolo']);
	$result->bindValue(':descricao',  "Protocolo Reaberto. Passo automático!");
	$result->bindValue(':passo',  $passo);
	$result->bindValue(':situacao',  "Em Progresso");
	$result->bindValue(':Setor_id_Origem', $_GET['origem']);
	$result->bindValue(':Setor_id_Destino', $_GET['origem']);
	$result->bindValue(':Usuario_Recebeu', $_GET['id_resp']);
	$data = date("Y-m-d H:i:s");
	$result->bindValue(':data_cadastro', $data);
	if($result->execute()){
		$sqlsss = 'update protocolo set setor_atual="'.$_GET['origem'].'", situacao="Em Progresso" where id='.$_GET['id_protocolo'];
		$conn->query($sqlsss);
		auditoria($_GET['id_resp'],"Cadastro","Reabriu Protocolo",$_GET['id_protocolo'],$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']),"");
		retorno("sim", "nenhum", $_GET['id_protocolo'], "");
	}else{
		retorno("nao", "banco", "", "");
	}

}

function cad_aviso(){
	require "Conexao.class.php";
	$result = $conn->prepare("INSERT INTO aviso (descricao, data_cadastro, cadastrado_por, tipo)
			VALUES (:descricao, :data_cadastro, :cadastrado_por, :tipo)");
	$result->bindValue(':descricao', $_POST['descricao']);
	$result->bindValue(':cadastrado_por',  $_GET['usuario_atual']);
	$result->bindValue(':tipo',  $_POST['tipo']);
	$data = date("Y-m-d H:i:s");
	$result->bindValue(':data_cadastro', $data);
	if($result->execute()){
		$sql_consulta = 'select * from aviso order by id desc LIMIT 1';
			
		foreach ($conn->query($sql_consulta) as $row) {
			$id = $row['id'];}
		auditoria($_GET['id_atual'],"Cadastro","Aviso",$id,$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']),"");
		retorno("sim", "nenhum", "", "");
	}else{
		retorno("nao", "banco", "", "");
	}

}

function cad_aviso_ciencia(){
	require "Conexao.class.php";
	$result = $conn->prepare("INSERT INTO aviso_ciente (id_aviso, data_cadastro, cadastrado_por)
			VALUES (:id_aviso, :data_cadastro, :cadastrado_por)");
	$result->bindValue(':id_aviso', $_GET['id_aviso']);
	$result->bindValue(':cadastrado_por',  $_GET['usuario_atual']);
	$data = date("Y-m-d H:i:s");
	$result->bindValue(':data_cadastro', $data);
	if($result->execute()){
		auditoria($_GET['id_atual'],"Cadastro","Ciência de Aviso",$_GET['id_aviso'],$_SERVER['REMOTE_ADDR'],gethostbyaddr($_SERVER['REMOTE_ADDR']),"");
			retorno("sim", "nenhum", "", "");
	}else{
		retorno("nao", "banco", "", "");
	}

}
if($_GET['funcao']=="cad_pessoa") cad_pessoa();
if($_GET['funcao']=="cad_doacao") cad_doacao();

if($funcao=="cad_usuario") cad_usuario($id_resp,$_POST["nome"], $_POST["cpf"],$_POST["usuario"],$_POST["senha2"],$_POST["telefone"],$_POST["email"],$_POST["cargo"],$_POST["setor"]);
if($funcao=="cad_animal") cad_animal();
if($funcao=="cad_proprietario") cad_proprietario();
if($funcao=="cad_setor") cad_setor();
if($funcao=="cad_cargo") cad_cargo();
if($funcao=="cad_estoque_movimento") cad_estoque_movimento();
if($funcao=="cad_estoque") cad_estoque();
if($funcao=="cad_protocolo") cad_protocolo();
if($funcao=="cad_protocolo_tramitacao") cad_protocolo_tramitacao();
if($funcao=="cad_protocolo_receber") cad_protocolo_receber();
if($funcao=="cad_protocolo_fechar") cad_protocolo_fechar();
if($funcao=="cad_protocolo_reabrir") cad_protocolo_reabrir();
if($funcao=="cad_aviso") cad_aviso();
if($funcao=="cad_aviso_ciencia") cad_aviso_ciencia();

?>
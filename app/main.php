

<?php
function tempoAtras($data){
$dataatual = date('Y-m-d H:i:s');
		$tempo = strtotime($dataatual)-strtotime($data);
		
		//Verifica o tempo restante em segundos e depois transforma em dias, meses, anos, etc...
		if($tempo < 60) {
			$tempo = $tempo . ".segundos";
		} elseif($tempo > 59 && $tempo < 120) {
			$tempo = $tempo/60 . ".minuto";
		} elseif($tempo > 119 && $tempo < 3600) {
			$tempo = $tempo/60 . ".minutos";
		} elseif($tempo > 3599 && $tempo < 7200) {
			$tempo = $tempo/60/60 . ".hora";
		} elseif($tempo > 7199 && $tempo < 86400) {
			$tempo = $tempo/60/60 . ".horas";
		} elseif($tempo > 86399 && $tempo < 172800) {
			$tempo = $tempo/60/60/24 . ".dia";
		} elseif($tempo > 172799 && $tempo < 5184000) {
			$tempo = $tempo/60/60/24 . ".dias";
		} elseif($tempo > 5183999 && $tempo < 10368000) {
			$tempo = $tempo/60/60/24/30 . ".mês";
		} elseif($tempo > 10367999 && $tempo < 62208000) {
			$tempo = $tempo/60/60/24/30/12 . ".meses";
		} elseif($tempo > 62207999 && $tempo < 124416000) {
			$tempo = $tempo/60/60/24/30/12/12 . ".ano";
		} elseif($tempo > 124415999) {
			$tempo = $tempo/60/60/24/30/12/12 . ".anos";
		}
		
		//Retira os "quebrados" da divisão
		$tempo = explode('.', $tempo);
		
		echo "há " . $tempo['0'] . " " . $tempo['2'] . " atrás \n";
}
?>

<div id="content-wrapper">
		<ul class="breadcrumb breadcrumb-page">
			<div class="breadcrumb-label text-light-gray">Você está aqui: </div>
			<li><a href="#">Timeline</a></li>
			
		</ul>
		<div class="page-header">
			<div class="row">
			<h1 class="col-xs-12 col-sm-4 text-center text-left-sm"><i class="fa fa-dashboard page-header-icon"></i>&nbsp;&nbsp;Timeline</h1>
			</div>
		</div> <!-- / .page-header -->
		<div class="row">
		<div class="input-group input-group-lg">
					<span class="input-group-addon no-background"><i class="fa fa-search"></i></span>
					<input type="text" name="s" class="form-control" placeholder="Buscar por palavra chave">
					<span class="input-group-btn">
						<button class="btn" type="submit">Buscar</button>
					</span>
				</div> <!-- / .input-group -->
		</div>
		<br>
		<div class="row">

		<!-- 16. $COMMENTS =================================================================================

				Comments
-->
				<div class="panel widget-comments">
					<div class="panel-heading ">
						<span class="panel-title"><i class="panel-title-icon fa fa-comment-o"></i>Últimas</span>
					</div> <!-- / .panel-heading -->
					<div class="panel-body">

					<?php
					require "class/Conexao.class.php";
					$sql = 'SELECT * from doar d, pessoa p where d.idpessoa = p.idpessoa and d.status = 1 order by d.data_cadastro DESC';
					foreach ($conn->query($sql) as $row) {	
					?>
					<?php
								    	$fotos_donar_p = glob("view/img/doar/".$row['iddoar']."/*g.jpg");
								    	$fotos_donar_g = glob("view/img/doar/".$row['iddoar']."/*g.jpg");
								    	
								   
									
									if (file_exists($fotos_donar_p[0])) {
									  $imagem=substr($fotos_donar_p[0], 0);
									  $imagem2=substr($fotos_donar_g[0],0);
									} else {
									   $imagem="view/img/noimg.png";
									   $imagem2="view/img/noimg.png";
									}
								    	
						?>
					
<div class="comment">
							<?php echo "<a href=\"$imagem2\"  class=\" fancybox\">
									<img class=\"comment-avatar\" width=\"80px\" src=\"$imagem\" alt=\"\">" ?>
							<div class="comment-body">
								<div class="comment-by">
									<a href="#" title=""><?=$row['nome'];?></a> postou uma <a href="#" title=""><?php if($row['tipo']==1) echo "Donar"; else if($row['tipo']==2) echo "Solicitação"; else echo "Donar Tempo";?></a>
								</div>
								<div class="comment-text col-md-10">
									<b><?=$row["titulo"];?><b> - <?= substr(($row["descricao"]), 0, 100);?>
								</div>
								<div class="comment-text col-md-2" align="right">
									<a href="?ir=doacao&iddoar=<?php echo $row['iddoar']?>" id="detalhes" class="btn btn-rounded btn-labeled btn-info"><span class="btn-label icon fa  fa-info-circle"></span>Detalhes</a>&nbsp;&nbsp;
								</div>
								<div class="comment-actions">
									<?php 
										if($row['tipo']==1){
									?>
									<button id="indicar" class="btn btn-rounded btn-labeled btn-success"><span class="btn-label icon fa fa-hand-o-up"></span>Indicar</button>&nbsp;&nbsp;
									<?php }else if($row['tipo']==2){ ?>
									<button id="doar" class="btn btn-rounded btn-labeled btn-success"><span class="btn-label icon fa fa-heart"></span>Donar</button>&nbsp;&nbsp;
									<?php }else{ ?>
									<button id="doar" class="btn btn-rounded btn-labeled btn-success"><span class="btn-label icon fa  fa-group"></span>Participar</button>&nbsp;&nbsp;
									<?php } ?>
									<!--<a href="#"><i class="fa fa-times"></i>Remove</a>-->
									<span class="pull-right"><?php tempoAtras($row["data_cadastro"]); ?></span>
								</div>
							</div> <!-- / .comment-body -->
						</div> <!-- / .comment -->
						<HR>
					<div class="fb-share-button" 
        data-href="http://www.your-domain.com/your-page.html" 
        data-layout="button_count">
    </div>
						<?php } ?>


						<!-- Panel Footer -->
			<div class="panel-footer">
				<ul class="pagination">
					<li class="disabled"><a href="#">«</a></li>
					<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
					<!--<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>-->
					<li><a href="#">»</a></li>
				</ul>
			</div> <!-- / .panel-footer -->


					</div> <!-- / .panel-body -->
				</div> <!-- / .panel -->
<!-- /16. $COMMENTS -->

<!-- Modal -->
				<div id="myModalDoar" class="modal fade" tabindex="-1" role="dialog" class="display-none">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">Detalhes da Doação</h4>
							</div>
							<div class="modal-body">
							<!--<div class="row">
								<div class="col-md-3">Telefone de Contato: </div>
								<div class="col-md-9">
									<input type="text" class="form-control" placeholder="Telefone de Contato">
								</div>
							</div>-->
							<div class="row">
								<div class="col-md-3">Observação: </div>
								<div class="col-md-9">
									<input type="hidden" id="iddoar" value="8">
									<input type="hidden" id="idsolicitante" value="8">
									<textarea id="obsdoacao" class="form-control" rows="3"></textarea>
								</div>
							</div>
							</div> <!-- / .modal-body -->
							<div class="modal-footer">
								<a href="#" class="btn btn-default" id="enviar" data-dismiss="modal">Enviar</a>
								<!--<button type="button" class="btn btn-primary">Save changes</button>-->
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div> <!-- /.modal -->
				<!-- / Modal -->
<!-- Modal -->
				<div id="myModalIndicar" class="modal fade" tabindex="-1" role="dialog" class="display-none">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">Detalhes da Indicação</h4>
							</div>
							<div class="modal-body">
								<div class="row form-group">
									<div class="col-md-2">Nome: </div>
									<div class="col-md-10">
										<input type="text" id="nome" class="form-control">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-2">Telefone: </div>
									<div class="col-md-10">
										<input type="text" id="telefone" class="form-control">
									</div>
								</div>
								<fieldset>
								<legend>Endereço</legend>
								<div class="row form-group">
									<div class="col-md-2">Logradouro: </div>
									<div class="col-md-10">
										<input type="text" id="telefone" class="form-control">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-2">Número: </div>
									<div class="col-md-4">
										<input type="text" id="telefone" class="form-control">
									</div>
									<div class="col-md-2">Bairro: </div>
									<div class="col-md-4">
										<input type="text" id="telefone" class="form-control">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-2">CEP: </div>
									<div class="col-md-4">
										<input type="text" id="telefone" class="form-control">
									</div>
									<div class="col-md-2">Cidade: </div>
									<div class="col-md-4">
										<select class="form-control">
											<option>Rio Branco</option>
											<option>Acrelândia</option>
											<option>Bujari</option>
											<option>Xapuri</option>
										</select>
									</div>
								</div>
								</fieldset>
							</div> <!-- / .modal-body -->
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Salvar</button>
								<!--<button type="button" class="btn btn-primary">Save changes</button>-->
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div> <!-- /.modal -->
				<!-- / Modal -->


<script>
init.push(function () {
$( "#doar" ).click(function() {
	var iddoar = $(this).attr('data-rel');
	var idsolicitante = $(this).attr('rel');
	$("#iddoar").val(iddoar);
	$("#idsolicitante").val(idsolicitante);
  	$('#myModalDoar').modal('show');
//$('#myModal').modal('hide');
});
$( "#enviar" ).click(function() {	
	var iddoacao = $('#iddoar').val();
	var idsolicitante = $('#idsolicitante').val();
	var obs = $('#obsdoacao').val();
	$('#myModalDoar').modal('hide');
	/*$.ajax({
			type: "POST",
			data: {idoacao: iddoacao, obs: obs, idsolicitante: idsolicitante},
			url: "class/enviarDoacao.php",
			dataType: "json",
			success: function(json){
				alert(json);
				if( json.msg == 'success' ){
					alert("Enviado com sucesso!");
				}else{

				}
			}
		});*/
   /*projetouniversal.util.getjson({
          url : "class/enviarDoacao.php",
          data : {idocacao: iddoacao, obs: obs, idsolicitante: idsolicitante},
          success : onSuccessSend,
          error : onError
        });*/
});
function onSuccessSend(obj){
	if( obj.msg == 'success' ){
		alert("Enviado com sucesso!");
	}else{

	}
}
function onError(obj){
	console.log(obj);
}
$( "#indicar" ).click(function() {
  $('#myModalIndicar').modal('show');
//$('#myModal').modal('hide');
});
});
</script>


		</div>
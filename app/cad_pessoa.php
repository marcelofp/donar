<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Cadastrar no Donar</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<!-- Open Sans font from Google CDN -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

	<!-- Pixel Admin's stylesheets -->
	<link href="assets/stylesheets/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/pixel-admin.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/widgets.min.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/pages.min.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/themes.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/principal.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/jquery.fancybox.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/flexslider.css" rel="stylesheet" type="text/css">
	<link href="assets/stylesheets/dataTables.responsive.css" rel="stylesheet" type="text/css">

	<!--[if lt IE 9]>
		<script src="assets/javascripts/ie.min.js"></script>
	<![endif]-->


<!-- $DEMO =========================================================================================

	Remove this section on production
-->
<!-- / $DEMO -->

</head>


<!-- 1. $BODY ======================================================================================
	
	Body

	Classes:
	* 'theme-{THEME NAME}'
	* 'right-to-left'     - Sets text direction to right-to-left
-->
<body class="theme-default page-signup">

<script>
	var init = [];
	
</script>


	<!-- Page background -->
	<div id="page-signup-bg">
		<!-- Background overlay -->
		<div class="overlay"></div>
		<!-- Replace this with your bg image -->
		<img src="assets/demo/signin-bg-1.jpg" alt="">
	</div>
	<!-- / Page background -->

	<!-- Container -->
	<div class="signup-container">
		<!-- Header -->
		<div class="signup-header">
			<a href="#" class="logo">
				<img src="view/img/donar.png" alt="" style="margin-top: -5px;">&nbsp;
				Donar
			</a> <!-- / .logo -->
			<div class="slogan">
				Ajude quem ajuda!
			</div> <!-- / .slogan -->
		</div>
		<!-- / Header -->

		<!-- Form -->
		<div class="signup-form">
			<form action="#" method="post" id="cadastro" >
				
				<div class="signup-text">
					<span>Crie sua conta!</span>
				</div>

				<div class="form-group w-icon">
					<input type="text" name="nome" id="nome" class="form-control input-lg" placeholder="Nome Completo">
					<span class="fa fa-info signup-form-icon"></span>
				</div>

				<div class="form-group w-icon">
					<input type="text" name="email" id="email" class="form-control input-lg" placeholder="E-mail">
					<span class="fa fa-envelope signup-form-icon"></span>
				</div>

				<div class="form-group w-icon">
					<input type="text" name="telefone" id="telefone" class="form-control input-lg" placeholder="Telefone">
					<span class="fa fa-user signup-form-icon"></span>
				</div>

				<div class="form-group w-icon">
					<input type="password" name="senha" id="senha" class="form-control input-lg" placeholder="Senha">
					<span class="fa fa-lock signup-form-icon"></span>
				</div>
				
				

				<div class="form-group" style="margin-top: 20px;margin-bottom: 20px;">
					<label class="checkbox-inline">
						<input type="checkbox" name="signup_confirm" class="px" id="confirm_id">
						<span class="lbl">Eu aceito todos os <a href="#" target="_blank">Termos e Condições.</a></span>
					</label>
				</div>

				<div class="form-actions">
					<input type="submit" value="CADASTRAR" class="signup-btn bg-primary">
				</div>
			</form>
			<!-- / Form -->

			<!-- "Sign In with" block -->
			<div class="signup-with">
				<!-- Facebook -->
				<a href="#" class="signup-with-btn" style="background:#4f6faa;background:rgba(79, 111, 170, .8);">Cadastrar com <span>Facebook</span></a>
			</div>
			<!-- / "Sign In with" block -->
		</div>
		<!-- Right side -->
	</div>

		<div class="have-account">
		Já possui um conta? <a href="index.php">Acessar</a>
	</div>

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- Pixel Admin's javascripts -->
<script src="assets/javascripts/bootstrap.min.js"></script>
<script src="assets/javascripts/pixel-admin.js"></script>
<script src="assets/javascripts/bootstrap-hover-dropdown.js"></script>
<script src="assets/javascripts/ajax_get.data.js"></script>
<script src="assets/javascripts/jquery.ba-dotimeout.js"></script>
<script src="assets/javascripts/jquery.fancybox.js"></script>
<script src="assets/javascripts/dataTables.responsive.js"></script>
<script src="assets/javascripts/jquery.flexslider.js"></script>
<script src="class/funcoes.js"></script>

<script type="text/javascript">
	// Resize BG
	init.push(function () {
		var $ph  = $('#page-signup-bg'),
		    $img = $ph.find('> img');

		$(window).on('resize', function () {
			$img.attr('style', '');
			if ($img.height() < $ph.height()) {
				$img.css({
					height: '100%',
					width: 'auto'
				});
			}
		});

		$("#signup-form_id").validate({ focusInvalid: true, errorPlacement: function () {} });

		// Validate name
		
	});

	window.PixelAdmin.start(init);
</script>
<?php 
require "class/Conexao.class.php";
require "class/funcoes.php";
pop_msg("success","Sucesso!","Cadastrado efetuado com sucesso.","#","_cadastro");
pop_msg("danger","Erro!","O email informado já está cadastrado no DONAR.","#","_email");
pop_msg("danger","Erro!","Ocorreu um erro ao cadastrar!","#","_aviso");

?>
<script>
					init.push(function () {
						$("#cadastro").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: true,
							rules: {
								'nome': {
								  required: true
								},
								'email': {
									  required: true,
									  email: true
									},
								'telefone': {
										required: true
										},									
								'senha': {
										required: true
										}	
							},

							submitHandler: function(form) {
								$("#ajax-overlay").show();	
							    var formdata = $("#cadastro").serialize();										    
						    	$.ajax({
						    		type: "POST",
						    		url: "class/inserir.php?funcao=cad_pessoa",
						    		data: formdata,
						    		dataType: "json",
						    		success: function(json){
						    			$("#ajax-overlay").hide(); 
						    			if(json.sucesso=="sim"){							    	
							    				$('#pop_modal_success_cadastro').modal('show').data('bs.modal').isShown = false;			
							    				$("#ok_modal_cadastro").attr("href", "index.php");		
									    	
							    			}else{
								    			if(json.tipo_erro=="email_existe") $('#pop_modal_danger_email').modal('show');
								    			else $('#pop_modal_danger_aviso').modal('show');	
							    		}
						    		}
						    		});
					        }
							
						});
						
						
						
					});
				</script>
<div id="ajax-overlay"><div id="processando" ><span><img width="20px" src="view/img/load.gif"> Processando...<br><font color=gray size=1">Por favor, aguarde um instante</font></span></div></div> 
<div id="ajax-overlay_img"><div id="processando" ><span><img width="20px" src="view/img/load.gif"><font color=green> Enviando Arquivos...</font><br><div id="envioImg">
<div class="progress progress-striped active" align="center" style="height:20px;"><div class="progress-bar progress-bar-success" id="div_porcento" align="center" style="width: 1%;">
</div></div></div><font color=gray size=1">Por favor, aguarde um instante</font></span></div></div> 

</body>
</html>
